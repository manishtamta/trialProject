//
//  CardActionButton.swift
//  Growthfile
//
//  Created by Chandresh SIngh on 05/01/17.
//  Copyright © 2017 can. All rights reserved.
//

import UIKit

class CardActionButton: UIButton {

    var cardId: String?
    var actionString: String?
    var selectedCellIndex = Int()
}
