//
//  TransactionViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 28/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TransactionViewController: UIViewController,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
  
    
    var keyboardUpBool = Bool()
    var commentTextview = UITextView()
    var amountTextfield = UITextField()
    var accountArray = NSMutableArray()
    var accountTextfield = UITextField()
    var collectionView :UICollectionView!
    var selectedRadioButtonType = String()
    var dueDateUITextField = UITextField()
    var selectNameTableViewPopUp = UITableView()
    
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerAlertBackground: UIView!
    
    var performEditTransaction = false
    
    var cardId = String()
    var editAmount = String()
    var editDueDate = String()
    var editPaymentType = String()
    var editAccountName = String()
    var editComment = String()
    
    var selectedAccountId = Int16()
    var selectedFormattedDate = String()
    
    var isdropDownButtonClicked = false
    var dropDownButtonImageView: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        if cardId != "" {
            performEditTransaction = true
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        commentTextview.delegate = self
        datePickerView.isHidden = true
        datePickerAlertBackground.isHidden = true
        
        self.setAccountsList()

        
        submitView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        submitView.layer.borderWidth = 1
        submitView.layer.cornerRadius = 15
        
        //Set border color on cancelview
        cancelView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        cancelView.layer.borderWidth = 1
        cancelView.layer.cornerRadius = 15
        
        //Popup table for nameSelection
        selectNameTableViewPopUp.tag = 100
        selectNameTableViewPopUp.rowHeight = 30
        selectNameTableViewPopUp.delegate = self
        selectNameTableViewPopUp.dataSource = self
        selectNameTableViewPopUp.layer.borderWidth = 1
        selectNameTableViewPopUp.isScrollEnabled = true
        selectNameTableViewPopUp.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        self.tableView.addSubview(selectNameTableViewPopUp)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        let alertBackgroundViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.hideAlertOnClick(sender:)))
        datePickerAlertBackground.addGestureRecognizer(alertBackgroundViewTapGesture)
        
        datePickerView.layer.cornerRadius = 7
        datePickerView.clipsToBounds = true
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let numberToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        numberToolbar.barStyle = UIBarStyle.default
        numberToolbar.items = [UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TransactionViewController.doneWithNumberKeyboard))]
        numberToolbar.sizeToFit()
        
        amountTextfield.inputAccessoryView = numberToolbar
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.selectedFormattedDate = dateFormatter.string(from: datePicker.date)
        
        if performEditTransaction == true {
            let formattedDueDate = commonFunctionFileObj.dateToNSDate(timestamp: editDueDate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.dateFormat = "dd MMM yyyy"
            dueDateUITextField.text = dateFormatter.string(from: formattedDueDate as Date)
            
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.selectedFormattedDate = dateFormatter.string(from: formattedDueDate as Date)
            
            datePicker.date = formattedDueDate as Date
        } else {
            dueDateUITextField.text = commonFunctionFileObj.dateToString(date: NSDate())
        }
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if keyboardUpBool
        {
            if(UIScreen.main.bounds.size.height <= 568.0)
            {
                self.tableView.contentOffset = CGPoint(x: 0, y: 40)
                self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height+40)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        if(UIScreen.main.bounds.size.height <= 568.0)
        {
            self.tableView.contentOffset = CGPoint(x: 0, y: 0)
            self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height)
        }
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (tableView.tag == 100)
        {
            return accountArray.count
        }
        return 5
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell?
        if (tableView.tag == 100)
        {
            if (cell == nil)
            {
                cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "cellIdentifier")
            }
            let model = accountArray.object(at: indexPath.row) as! SelectAmountDataModel
            cell?.textLabel?.text = String(model.accountName)!
            cell?.textLabel?.textColor = UIColor.darkGray
            cell?.textLabel?.font = UIFont(name: "System", size: 12)
        }
        else
        {
            if indexPath.row == 0
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "typeIdentifier", for: indexPath as IndexPath)
                collectionView = cell?.viewWithTag(7) as! UICollectionView
            }
            else if indexPath.row == 1
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "accountIdentifier", for: indexPath as IndexPath)
                accountTextfield = cell?.viewWithTag(1) as! UITextField
                
                let showDropDownButton = cell?.viewWithTag(2) as! UIButton
                if performEditTransaction == true {
                    if editAccountName != "" {
                        accountTextfield.text = editAccountName
                    }
                    
                    accountTextfield.isUserInteractionEnabled = false
                    showDropDownButton.isEnabled = false
                } else {
                    
                    self.dropDownButtonImageView = cell?.viewWithTag(111) as! UIImageView
                    showDropDownButton.addTarget(self, action: #selector(TransactionViewController.showDropDownButtonAction(sender:)), for: UIControlEvents.touchUpInside)
                }
            }
            else if indexPath.row == 2
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "amountIdentifier", for: indexPath as IndexPath)
                amountTextfield = cell?.viewWithTag(6) as! UITextField
                
                if performEditTransaction == true {
                    amountTextfield.text = editAmount
                }
            }
            else if indexPath.row == 3
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "dueDateIdentifier", for: indexPath as IndexPath)
                dueDateUITextField = cell?.viewWithTag(1) as! UITextField
                
                let showDatepickerviewButton = cell?.viewWithTag(2) as! UIButton
                showDatepickerviewButton.addTarget(self, action: #selector(ResignViewController.showDatepickerviewButtonAction), for: UIControlEvents.touchUpInside)
            }
            else if indexPath.row == 4
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "commentIdentifier", for: indexPath as IndexPath)
                
                commentTextview = cell?.viewWithTag(1) as! UITextView
                commentTextview.layer.cornerRadius = 5;
                commentTextview.layer.borderWidth = 1
                commentTextview.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
                
                if performEditTransaction == true && editComment != ""{
                    self.commentTextview.text = editComment
                }
            }
        }
        
        return cell!
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (tableView.tag == 100)
        {
            let model = accountArray.object(at: indexPath.row) as! SelectAmountDataModel
            accountTextfield.placeholder = ""
            accountTextfield.text = String(model.accountName)!

            self.selectedAccountId = model.accountId
            
            selectNameTableViewPopUp.isHidden = true
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (tableView.tag == 100)
        {
            return 30
        }
        else
        {
            if indexPath.row == 0
            {
                return 40
            }
            else if indexPath.row == 1 || indexPath.row == 2
            {
                return 47
            }
            else if indexPath.row == 3
            {
                return 57
            }
            else
            {
                return 149
            }
        }
    }
    
    
    
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 2
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var cell =  UICollectionViewCell()
        
        if indexPath.row == 0
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionsCell", for: indexPath as IndexPath)
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.top)
            let selectedCellImageView = cell.viewWithTag(1) as! UIImageView
            if performEditTransaction == true {
                if editPaymentType == "REVENUE" {
                    selectedCellImageView.backgroundColor = UIColor(red: 0/255.0, green: 178.0/255.0, blue: 235.0/255.0, alpha:1.0)
                    selectedRadioButtonType = "Collection"
                }
            }
            
            selectedCellImageView.layer.cornerRadius = (selectedCellImageView.frame.size.height)/2
            selectedCellImageView.layer.masksToBounds = true
            selectedCellImageView.layer.borderColor = UIColor.lightGray.cgColor
            selectedCellImageView.layer.borderWidth = 1.0
        }
        else
        {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentIdentifier", for: indexPath as IndexPath)
            let selectedCellImageView = cell.viewWithTag(1) as! UIImageView
            if performEditTransaction == true {
                if editPaymentType == "EXPENSE" {
                    selectedCellImageView.backgroundColor = UIColor(red: 0/255.0, green: 178.0/255.0, blue: 235.0/255.0, alpha:1.0)
                    selectedRadioButtonType = "Payment"
                }
            }
            selectedCellImageView.layer.cornerRadius = (selectedCellImageView.frame.size.height)/2
            selectedCellImageView.layer.masksToBounds = true
            selectedCellImageView.layer.borderColor = UIColor.lightGray.cgColor
            selectedCellImageView.layer.borderWidth = 1.0
        }
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if performEditTransaction != true {
            let cell = collectionView.cellForItem(at: indexPath)
            let selectedCellImageView = cell?.viewWithTag(1) as! UIImageView
            selectedCellImageView.backgroundColor = UIColor(red: 0/255.0, green: 178.0/255.0, blue: 235.0/255.0, alpha:1.0)
            let typeNameLabel = cell?.viewWithTag(2) as! UILabel
        
            selectedRadioButtonType = typeNameLabel.text!
        }
        print("selectedRadioButtonType \(selectedRadioButtonType)")
    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        if performEditTransaction != true {
            let cell = collectionView.cellForItem(at: indexPath)
            let selectedCellImageView = cell?.viewWithTag(1) as! UIImageView
            selectedCellImageView.backgroundColor = UIColor.white
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 110, height: 50)
    }
    
    
    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //Set maximum limit for mobileNumberTextField to 10
        guard let text = textField.text else { return true }
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 9
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        keyboardUpBool = false
    }
    
    
    
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        keyboardUpBool = true
        amountTextfield.resignFirstResponder()
        return true
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        selectNameTableViewPopUp.isHidden = true
        if textView.text == "Write a comment..."
        {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty
        {
            textView.text = "Write a comment..."
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.endEditing(true)
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return numberOfChars <= 200
 
    }
    
    
    
    func doneWithNumberKeyboard()
    {
        amountTextfield.resignFirstResponder()
    }
    
    func showDropDownButtonAction(sender: UIButton)
    {
        if accountArray.count == 0 {
            accountTextfield.placeholder = "No Account Exists"
        }
        else {
            if isdropDownButtonClicked == true {
                self.isdropDownButtonClicked = false
                self.dropDownButtonImageView.image = UIImage(named: "down_book_transaction.png") as UIImage!
                
                accountTextfield.text = nil
                accountTextfield.placeholder = "Select Account"
                selectNameTableViewPopUp.isHidden = true
            }
            else {
                self.isdropDownButtonClicked = true
                self.dropDownButtonImageView.image = UIImage(named: "cross.png") as UIImage!
                
                commentTextview.endEditing(true)
                amountTextfield.resignFirstResponder()
                
                var tableHeight = 0
                if accountArray.count < 5
                {
                    tableHeight = accountArray.count * 30
                }
                else
                {
                    tableHeight = 150
                }
                let selectnameLabelOriginInTableView =  accountTextfield.convert(accountTextfield.frame.origin, to: tableView)
                selectNameTableViewPopUp.frame =  CGRect(x: 93, y: selectnameLabelOriginInTableView.y + accountTextfield.frame.size.height + 12, width: accountTextfield.frame.size.width + 45, height: CGFloat(tableHeight))
                selectNameTableViewPopUp.isHidden = false
            }
        }
    }
    
    
    
    func showDatepickerviewButtonAction()
    {
        commentTextview.endEditing(true)
        amountTextfield.resignFirstResponder()
        datePickerAlertBackground.isHidden = false
        datePickerView.isHidden = false
        self.datePickerAlertBackground.backgroundColor = UIColor.darkGray.withAlphaComponent(0.9)
    }
    
    func hideAlertOnClick(sender:UITapGestureRecognizer)
    {
        datePickerView.isHidden = true
        datePickerAlertBackground.isHidden = true
    }
    
    
    
    @IBAction func submitAction(_ sender: Any)
    {
        if performEditTransaction == true {
            editTransaction()
        }
        else {
            
            if selectedRadioButtonType != "Payment" && selectedRadioButtonType != "Collection" {
                showAlertMessage(msg: "Please select payment type")
                return
            }
            
            if amountTextfield.text == "" {
                showAlertMessage(msg: "Amount Required")
                return
            }
            if Int(amountTextfield.text!)! <= 0 {
                showAlertMessage(msg: "Positive amount value required")
                return
            }
            if dueDateUITextField.text == nil {
                showAlertMessage(msg: "Due Date Required")
                return
            }
            
            
            self.bookTransaction()
        }
    }
    
    @IBAction func cancelAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneButtonAction(_ sender: Any)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd MMM yyyy"
        let selecteddateOfDatepicker = dateFormatter.string(from: datePicker.date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.selectedFormattedDate = dateFormatter.string(from: datePicker.date)
        dueDateUITextField.text = selecteddateOfDatepicker
        datePickerView.isHidden = true
        datePickerAlertBackground.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func setAccountsList() {
        
        self.loader.isHidden = false
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        print(token)
        if token == "" {
            // show error message
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        
        Alamofire.request("\(Constants.domain)get/list/accounts", method: .get, headers: headers)
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["response"]["data"] != JSON.null
                        {
                            let accountsList = json["response"]["data"]
                            
                            for (_,account):(String, JSON) in accountsList {
                                let model = SelectAmountDataModel()
                                model.accountName = account["account_name"].stringValue
                                model.accountId = Int16(account["id"].stringValue)!
                                self.accountArray.add(model)
                            }
                        } else if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            if message != "Object not found." {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                   self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Invalid credentials")
                    return
                }
        }
        
    }
    
    
    
    func bookTransaction() {
        
        loader.isHidden = false
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        var params: Parameters = [
            "due_date": self.selectedFormattedDate,
            "amount": Int(self.amountTextfield.text!),
        ]
        
        print("selected: \(self.selectedRadioButtonType)")
        
        if self.selectedRadioButtonType == "Payment"
        {
            params["status"] = "EXPENSE"
        }
        else
        {
            params["status"] = "REVENUE"
        }
        
        
        let comment = self.commentTextview.text
        if comment != "Write a comment..." {
            params["comment"] = comment
        }
        
        if self.selectedAccountId > 0 {
            params["account_id"] = String(self.selectedAccountId)
        }
        
        
        Alamofire.request("\(Constants.domain)employees/booktransaction", method: .post, parameters: params, headers: headers)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            if response.response?.statusCode == 200 {
                                self.showSucessAlertMessage(msg: message)
                            }
                            else {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                    self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Invalid credentials")
                    return
                }
        }
    }
    
    func editTransaction()
    {
        loader.isHidden = false
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        var params: Parameters = [
            "card_id": self.cardId
            ]

        if self.amountTextfield.text != nil {
            params["due_date"] = self.selectedFormattedDate
        }
        
        if editAmount != self.amountTextfield.text && self.amountTextfield.text != nil {
            params["amount"] = self.amountTextfield.text
        }
        
        
        let comment = self.commentTextview.text
        if (comment != nil) && (comment != "Write a comment...") && (comment != editComment) {
            params["comment"] = comment
        }
        
        if self.selectedFormattedDate != "" {
            params["due_date"] = self.selectedFormattedDate
        }
        
        
        Alamofire.request("\(Constants.domain)employees/transactions", method: .patch, parameters: params, headers: headers)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        let json = JSON(JSONdata)
                        if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            
                            if response.response?.statusCode == 200 {
                                self.showSucessAlertMessage(msg: message)
                            }
                            else {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                    self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Ops.. Something went wrong")
                    return
                }
        }
    }
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSucessAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: cancelAction(_:)))
        self.present(alert, animated: true, completion: nil)
    }
    
}
