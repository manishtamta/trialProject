//
//  ResignViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 28/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ResignViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
    
   
    var nameArray = NSMutableArray()
    var selectedDateLabel = UILabel()
    var commentTextview = UITextView()

    var selectedDate = NSDate()
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePickerAlertBackground: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
        tableView.delegate = self
        tableView.dataSource = self
        commentTextview.delegate = self
        datePickerAlertBackground.isHidden = true
        datePickerView.isHidden = true
        
        datePickerView.layer.cornerRadius = 7
        datePickerView.clipsToBounds = true
       
        submitView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        submitView.layer.borderWidth = 1
        submitView.layer.cornerRadius = 15
        
        //Set border color on cancelview
        cancelView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        cancelView.layer.borderWidth = 1
        cancelView.layer.cornerRadius = 15
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let array = ["Name 1","Name 2","Name 3","Name 4","Name 5"]
        for dic in array
        {
            let model = SelectAmountDataModel()
            model.accountName = dic
            self.nameArray.add(model)
        }
        
        let alertBackgroundViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.hideAlertOnClick(sender:)))
        datePickerAlertBackground.addGestureRecognizer(alertBackgroundViewTapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        let date = Calendar.current.date(byAdding: .day, value: 1, to: NSDate() as Date)!
        
        let maxDate = Calendar.current.date(byAdding: .day, value: 90, to: NSDate() as Date)
        datePicker.maximumDate = maxDate
        
        datePicker.minimumDate = date as Date
        datePicker.maximumDate = maxDate! as Date
        
        self.selectedDate = date as NSDate
        selectedDateLabel.text = commonFunctionFileObj.dateToString(date: date as NSDate)
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(UIScreen.main.bounds.size.height <= 568.0)
        {
            self.tableView.contentOffset = CGPoint(x: 0, y: 40)
            self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height+40)
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        if(UIScreen.main.bounds.size.height <= 568.0)
        {
            self.tableView.contentOffset = CGPoint(x: 0, y: 0)
            self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height)
        }
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell:UITableViewCell?
      
        if indexPath.row == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "lastDateIdentifier", for: indexPath as IndexPath)
            selectedDateLabel = cell?.viewWithTag(1) as! UILabel
                
            let showDatepickerviewButton = cell?.viewWithTag(2) as! UIButton
            showDatepickerviewButton.addTarget(self, action: #selector(ResignViewController.showDatepickerviewButtonAction), for: UIControlEvents.touchUpInside)
        }
        else if indexPath.row == 1
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "commentIdentifier", for: indexPath as IndexPath)
                
            commentTextview = cell?.viewWithTag(1) as! UITextView
            commentTextview.layer.cornerRadius = 5;
            commentTextview.layer.borderWidth = 1
            commentTextview.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        }
        
        return cell!
    }
    
  
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       
        if indexPath.row == 0
        {
            return 57
        }
        else
        {
            return 149
        }
    }
    
    
    
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Write a comment..."
        {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty
        {
            textView.text = "Write a comment..."
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.endEditing(true)
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return numberOfChars <= 200
    }
    
   
    func showDatepickerviewButtonAction()
    {
        datePickerAlertBackground.isHidden = false
        datePickerView.isHidden = false
        self.datePickerAlertBackground.backgroundColor = UIColor.darkGray.withAlphaComponent(0.9)
    }
    
    
    func hideAlertOnClick(sender:UITapGestureRecognizer)
    {
        datePickerView.isHidden = true
        datePickerAlertBackground.isHidden = true
    }
    
    @IBAction func doneButton(_ sender: Any)
    {
        selectedDateLabel.text = commonFunctionFileObj.dateToString(date: datePicker.date as NSDate)
        self.selectedDate = datePicker.date as NSDate
        datePickerView.isHidden = true
        datePickerAlertBackground.isHidden = true
    }
    
    
    @IBAction func cancelAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any)
    {
        if self.commentTextview.text == "Write a comment..." || self.commentTextview.text == "" {
            self.showAlertMessage(msg: "Please write a comment")
        } else {
            resignation()
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func resignation()
    {
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        let params: Parameters = [
            "date": commonFunctionFileObj.dateToSupportedFormatString(date: selectedDate as NSDate),
            "remark": self.commentTextview.text
            ]
        
        Alamofire.request("\(Constants.domain)employees/resign", method: .post, parameters: params, headers: headers).validate(statusCode: 200..<450)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            if response.response?.statusCode == 200 {
                                self.showSucessAlertMessage(msg: message)
                            }
                            else {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                    self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Ops... Something went wrong")
                    return
                }
        }
        
    }
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSucessAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: cancelAction(_:)))
        self.present(alert, animated: true, completion: nil)
    }
    
}
