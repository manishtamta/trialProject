//
//  User+CoreDataProperties.swift
//  Growthfile
//
//  Created by Chandresh SIngh on 29/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User");
    }

    @NSManaged public var name: String?
    @NSManaged public var profile_image: String?
    @NSManaged public var email: String?
    @NSManaged public var mobile: String?
    @NSManaged public var token: String?

}
