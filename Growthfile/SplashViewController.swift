//
//  ViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 21/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController
{
    
    @IBOutlet weak var agreeAndContinueOutlet: UIButton!
    @IBOutlet weak var termsAndPrivatePolicyLabel: UILabel!
    @IBOutlet weak var termsAndServiceButton: UIButton!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //Set termsAndPrivatePolicyLabel textcolor
        termsAndPrivatePolicyLabel.textColor = UIColor(red: 63.0/255.0, green: 151.0/255.0, blue: 207.0/255.0, alpha:1.0)
        self.termsAndServiceButton.setTitleColor(UIColor(red: 63.0/255.0, green: 151.0/255.0, blue: 207.0/255.0, alpha:1.0), for: .normal)
        
        self.privacyPolicyButton.setTitleColor(UIColor(red: 63.0/255.0, green: 151.0/255.0, blue: 207.0/255.0, alpha:1.0), for: .normal)
        
        
        //Set corner radius and border color of agreeAndContinueButton
        agreeAndContinueOutlet.layer.cornerRadius = 16
        agreeAndContinueOutlet.layer.borderWidth = 1
        agreeAndContinueOutlet.layer.borderColor = UIColor(red: 80/255, green: 190/255, blue: 237/255, alpha: 1).cgColor
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func agreeAndContinueAction(_ sender: Any)
    {
        //Push to login viewcontroller
        let loginViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        self.navigationController?.pushViewController(loginViewControllerObj!, animated: true)
    }

    @IBAction func termsAndServiceButtonAction(_ sender: UIButton) {
        let tAndSControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController
        tAndSControllerObj?.Url = Constants.growthfileTermsAndConditionsSiteUrl
        tAndSControllerObj?.headerTitle = "Terms & Conditions"
        self.navigationController?.pushViewController(tAndSControllerObj!, animated: true)
    }
    
    
    @IBAction func privacyPolicyButtonAction(_ sender: UIButton) {
        let tAndSControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "TermsAndConditionViewController") as? TermsAndConditionViewController
        tAndSControllerObj?.Url = Constants.growthfilePrivacyPolicySiteUrl
        tAndSControllerObj?.headerTitle = "Privacy Policy"
        self.navigationController?.pushViewController(tAndSControllerObj!, animated: true)
    }

    
}

