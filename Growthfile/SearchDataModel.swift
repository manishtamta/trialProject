//
//  searchDataModel.swift
//  Growthfile
//
//  Created by Preeti Sharma on 26/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit

class SearchDataModel: NSObject
{
    var id = Int32()
    var name = String()
    var designation = String()
    var profileImage = String()
}
