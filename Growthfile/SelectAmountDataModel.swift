//
//  SelectAmountDataModel.swift
//  Growthfile
//
//  Created by Preeti Sharma on 28/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit

class SelectAmountDataModel: NSObject
{
    var accountName = String()
    var accountId = Int16()
}
