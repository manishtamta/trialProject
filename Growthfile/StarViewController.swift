//
//  StarViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 29/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StarViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,SelectedDataDelegate,UITextViewDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
   
    
    var dataArray  = NSMutableArray()
    var commentTextview = UITextView()
    var collectionView :UICollectionView!
    var pushToSearchButton = UIButton()
    var heightOfCollectionViewCell = CGFloat()
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       
        
        tableView.delegate = self
        tableView.dataSource = self
        commentTextview.delegate = self
        
        submitView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        submitView.layer.borderWidth = 1
        submitView.layer.cornerRadius = 15
        
        //Set border color on cancelview
        cancelView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        cancelView.layer.borderWidth = 1
        cancelView.layer.cornerRadius = 15
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        dataArray.removeAllObjects()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        adjustTableviewHeight()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        if indexPath.row == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "starCellIdentifier", for: indexPath as IndexPath)
            collectionView = cell.viewWithTag(20) as! UICollectionView
            pushToSearchButton = cell.viewWithTag(1000) as! UIButton
        }
        else
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "commentIdentifier", for: indexPath as IndexPath)
            
            commentTextview = cell.viewWithTag(1) as! UITextView
            commentTextview.layer.cornerRadius = 5;
            commentTextview.layer.borderWidth = 1
            commentTextview.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        }
        
        return cell
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return heightOfCollectionViewCell
        }
        return 149
    }
    
    
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return dataArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "starCollectionCellIdentifier", for: indexPath as IndexPath)
        
        let nameLabel = cell.viewWithTag(12) as! UILabel
        
        let selectedObject = dataArray.object(at: indexPath.row) as! SearchDataModel
        
        nameLabel.text = selectedObject.name
        
        if  (cell.viewWithTag(1) != nil)
        {
            let deletebutton = cell.viewWithTag(1) as! UIButton
            //deletebutton.tag = indexPath.row + 1
        }
        
        let backgroundUIView = cell.viewWithTag(10)
        backgroundUIView!.layer.cornerRadius = 13
        backgroundUIView!.clipsToBounds = true
        backgroundUIView?.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        backgroundUIView?.layer.borderWidth = 1
        
        
        let fullName = selectedObject.name
        
        let fullNameArr = fullName.characters.split{$0 == " "}.map(String.init)
        
        var str = String(describing: nameLabel.text!.characters.first!).capitalized
        
        let isIndexValid = fullNameArr.indices.contains(1)
        
        if isIndexValid == true {
            let lastName:String = fullNameArr[1]
            str = String(describing: nameLabel.text!.characters.first!).capitalized +  String(describing: lastName.characters.first!).capitalized
        }
        
        let receiverNameFirstCharacterLabel = cell.viewWithTag(11) as! UILabel
        receiverNameFirstCharacterLabel.text! = str
        receiverNameFirstCharacterLabel.layer.cornerRadius = receiverNameFirstCharacterLabel.frame.size.width/2
        receiverNameFirstCharacterLabel.clipsToBounds = true
        return cell
    }
    
//    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
//    {
//        print("didSelectItemAtdidSelectItemAtdidSelectItemAt")
//        let searchViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
//        searchViewControllerObj?.isSelectDataNNotMoveToProfileScreenBool = true
//        searchViewControllerObj!.delegate = self
//        self.navigationController?.pushViewController(searchViewControllerObj!, animated: false)
//    }
    
    public func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath)
        let selectedCellImageView = cell?.viewWithTag(1) as! UIImageView
        selectedCellImageView.backgroundColor = UIColor.white
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let selectedObject = dataArray.object(at: indexPath.row) as! SearchDataModel
        let nameString = selectedObject.name
//        let myString: NSString = nameLabel.text! as NSString
//        
//        let nameString =  String(describing: dataArray.object(at: indexPath.row))
        let myString: NSString = nameString as NSString
        let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        return CGSize(width:size.width + 58, height: 28)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets
    {
        print("insetForSectionAt")
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 25)
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Write a comment..."
        {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty
        {
            textView.text = "Write a comment..."
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.endEditing(true)
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return numberOfChars <= 200
    }
    
   
    func selectedDataArrayFromSearchViewController(selectedDataArray: NSMutableArray)
    {
        let newItemString = String(describing: (selectedDataArray[0] as! SearchDataModel).name) as NSString
        var itemPresent = false

        for item in dataArray {
            let itemString = String(describing: (item as! SearchDataModel).name) as NSString
            if itemString == newItemString {
                itemPresent = true
                break
            }
        }
        if itemPresent == false {
            dataArray.add(selectedDataArray[0])
            self.collectionView.reloadData()
        }
    }
    
    func adjustTableviewHeight()
    {
        var height = CGFloat(50)
        var largestCellwidth = CGFloat(0)
        for item in dataArray
        {

            //nameLabel.text =
            let myString: NSString = String(describing: (item as! SearchDataModel).name) as NSString
            
            //let myString: NSString = item as! NSString
            let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
            
            if largestCellwidth < size.width + 55
            {
                largestCellwidth = size.width + 60
            }
            
            height = height + 28
            self.collectionView.frame = CGRect(x: 35,y: 40,width: largestCellwidth,height: height)
            collectionView.translatesAutoresizingMaskIntoConstraints = true
        }
        
        heightOfCollectionViewCell = height
        
        tableView.estimatedRowHeight = 700
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.reloadData()
        self.collectionView.reloadData()
        
        if dataArray.count == 6
        {
            pushToSearchButton.isHidden = true
        }
        else
        {
            pushToSearchButton.isHidden = false
        }
    }
    
    @IBAction func pushToSearchViewController(_ sender: Any)
    {
        let searchViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
        searchViewControllerObj?.isSelectDataNNotMoveToProfileScreenBool = true
        searchViewControllerObj?.isStarOrSmileySearch = true
        searchViewControllerObj!.delegate = self
        self.navigationController?.pushViewController(searchViewControllerObj!, animated: false)
    }
    
    @IBAction func deleteDataButtonAction(_ sender: Any)
    {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: collectionView)
        var indexPath = collectionView.indexPathForItem(at: buttonPosition)
        dataArray.removeObject(at: (indexPath?.row)!)
        adjustTableviewHeight()
        collectionView.reloadData()
        tableView.reloadData()
        print("dataArraydataArrayrer \(indexPath?.row)")
        print("dataArraydataArrayrer \(dataArray)")
    }
    
    @IBAction func submitViewAction(_ sender: Any)
    {
        if self.commentTextview.text == "Write a comment..."
        {
            self.showAlertMessage(msg: "Comment is required")
        }
        else
        {
            self.sendSmiley()
        }
    }
    
    @IBAction func cancelAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func sendSmiley()
    {
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        let idsCount = self.dataArray.count
        
        var selectedReceiverIds: [Int32] = []
        
        var i = 0
        while i < idsCount {
            let selectedItemObj = dataArray.object(at: i) as! SearchDataModel
            print(selectedItemObj.id)
            selectedReceiverIds.append(selectedItemObj.id)
            i = i + 1
        }

        
        if idsCount > 0 {
            let params: Parameters = [
                "receiver_ids" : selectedReceiverIds,
                "comment": commentTextview.text
            ]
            
            Alamofire.request("\(Constants.domain)employees/sendstar", method: .post, parameters: params, headers: headers)
                .responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    switch response.result
                    {
                    case .success:
                        if let JSONdata = response.result.value {
                            let json = JSON(JSONdata)
                            if json["msg"] != "" {
                                let message = json["msg"].stringValue;
                                if message == "Token expired" || message == "Token invalid" {
                                    defaultVal.set(false, forKey: Constants.authUserKey)
                                }
                                if response.response?.statusCode == 200 {
                                    self.showSucessAlertMessage(msg: message)
                                }
                                else {
                                    self.showAlertMessage(msg: message)
                                }
                            }
                        }
                        self.loader.isHidden = true
                    case .failure(let error):
                        print(error)
                        self.loader.isHidden = true
                        self.showAlertMessage(msg: "Ops... Something went wrong")
                        return
                    }
            }
            
        } else
        {
            self.loader.isHidden = true
            self.showAlertMessage(msg: "Please select a User")
        }
    }
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSucessAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: cancelAction(_:)))
        self.present(alert, animated: true, completion: nil)
    }
   
}
