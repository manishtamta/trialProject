//
//  CommonFunctionFile.swift
//  Growthfile
//
//  Created by Preeti Sharma on 29/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit

class CommonFunctionFile: NSObject
{
    //ACTIVITYINDICATOR FUNCTIONS
//    func createActivityIndicatorInAllViewController(view:UIView) -> (activityIndicatorContainerView:UIView , activityIndicator:UIActivityIndicatorView)
//    {
//        let activityIndicatorContainerView = UIView()
//        activityIndicatorContainerView.isHidden = true
//        activityIndicatorContainerView.frame = view.frame
//        view.addSubview(activityIndicatorContainerView)
//        
//        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
//        activityIndicator.center = view.center
//        activityIndicator.hidesWhenStopped = true
//        activityIndicatorContainerView.addSubview(activityIndicator)
//        
//        return (activityIndicatorContainerView , activityIndicator)
//    }
//    
//    func activityIndicatorShowAndStartAnimating(activityIndicator:UIActivityIndicatorView,activityIndicatorContainerView:UIView)
//    {
//        activityIndicatorContainerView.isHidden = false
//        activityIndicator.startAnimating()
//    }
//    
//    func activityIndicatorHideAndStopAnimating(activityIndicator:UIActivityIndicatorView,activityIndicatorContainerView:UIView)
//    {
//        activityIndicator.stopAnimating()
//        activityIndicatorContainerView.isHidden = true
//    }
//  gghjjhhjhjjgnjfjhjb
    func createLoader(view:UIView) -> UIView
    {
        let loaderBackgroundView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        loaderBackgroundView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        
        let loaderImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        loaderImageView.image = UIImage(named: "growthfile_nav.png")
        loaderImageView.backgroundColor = UIColor.clear
        loaderImageView.center = view.center
        
        let loaderLineView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        loaderLineView.backgroundColor = UIColor.clear
        loaderLineView.center = view.center
        
        loaderBackgroundView.addSubview(loaderImageView)
        loaderBackgroundView.addSubview(loaderLineView)
        view.addSubview(loaderBackgroundView)
        
        createCustomLoaderInAllViewController(activityIndicator: loaderLineView, activityIndicatorImage: loaderImageView )
        
        return loaderBackgroundView
    }
    
    func createCustomLoaderInAllViewController(activityIndicator : UIView ,activityIndicatorImage : UIImageView )
    {
        activityIndicator.layer.cornerRadius = activityIndicator.bounds.size.width/2
        activityIndicator.clipsToBounds = true
        activityIndicatorImage.layer.cornerRadius = activityIndicatorImage.bounds.size.width/2
        activityIndicatorImage.clipsToBounds = true
        
        let barDataArray = [60,40]
        let barDataArrayColour = [UIColor.clear.cgColor,UIColor.white.cgColor]
        var startAngle:CGFloat = 0.0
        var endAngle:CGFloat = 0.0
        var numberOfArc = 0
        while numberOfArc < barDataArray.count
        {
            let arcPercentage = barDataArray[numberOfArc]
            
            let percentOfSix:CGFloat = 6.28319 * CGFloat(arcPercentage) / 100
            startAngle = endAngle
            endAngle = percentOfSix + startAngle
            
            let circlePath = UIBezierPath(arcCenter: CGPoint(x: (activityIndicator.frame.size.width) / 2.0, y: (activityIndicator.frame.size.height) / 2.0), radius: ((activityIndicator.frame.size.width))/2, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = circlePath.cgPath
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = barDataArrayColour[numberOfArc]
            shapeLayer.lineWidth = 7.0
            activityIndicator.layer.addSublayer(shapeLayer)
            numberOfArc = numberOfArc + 1
        }
        
        activityIndicator.loader(duration: 1.5, completionDelegate: nil)
    }

    
  
    
    
    
    //DATE FUNCTION
    func dateToString(date:NSDate) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd MMM yyyy"
        let dateString = dateFormatter.string(from: date as Date)
        return dateString
    }
    
    
    func dateToSupportedFormatString(date:NSDate) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from: date as Date)
        return dateString
    }
    
   
    func timestampToCardTimeString(timestamp:String) -> String
    {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateStringFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let d = dateStringFormatter.date(from: timestamp)!
        
        dateStringFormatter.dateFormat = "dd/MM/yyyy"
        let currentDate = Date()
        if dateStringFormatter.string(from: d) == dateStringFormatter.string(from: currentDate) {
            dateStringFormatter.amSymbol = "am"
            dateStringFormatter.pmSymbol = "pm"
            dateStringFormatter.dateFormat = "hh:mma"
        }

        let dateOrtimeString = dateStringFormatter.string(from: d)
        return dateOrtimeString
    }
    
    func dateToNSDate(timestamp:String) -> NSDate
    {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        dateStringFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let d = dateStringFormatter.date(from: timestamp)!
        return d as NSDate
    }
    
    func NSDateToMonthDate(date:NSDate) -> String
    {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = "MMM"
        let monthName = dateStringFormatter.string(from: date as Date)
        return monthName
    }
    
    
    //SET LABEL OR IMAGE ON IMAGEVIEW
    func addImageOrNameLabelValueToCard(profileLabel:UILabel,profileImageview:UIImageView,imageString:String,nameString:String)
    {
        profileLabel.layer.cornerRadius = (profileLabel.frame.size.height)/2
        profileLabel.layer.masksToBounds = true
        profileLabel.backgroundColor = UIColor(red: 80.0/255.0, green: 190.0/255.0, blue: 237.0/255.0, alpha: 1)
        
        if imageString.characters.count == 0
        {
            profileLabel.isHidden = false
            profileImageview.isHidden = true
            profileLabel.text = getFirstCharacterFromProfileName(name: nameString)
        }
        else
        {
            profileLabel.isHidden = true
            profileImageview.isHidden = false
            profileImageview.sd_setImage(with: NSURL(string: imageString) as URL!)
        }
    }
    
    
    func getFirstCharacterFromProfileName(name:String) -> String
    {
        let fullNameArr = name.characters.split{$0 == " "}.map(String.init)
        
        var firstCharacterstring = String(describing: name.characters.first!).capitalized
        
        let isIndexValid = fullNameArr.indices.contains(1)
        
        if isIndexValid == true
        {
            let lastName:String = fullNameArr[1]
            firstCharacterstring = String(describing: name.characters.first!).capitalized +  String(describing: lastName.characters.first!).capitalized
        }
        
        return firstCharacterstring
    }
    
}


extension UIView
{
    func loader(duration: CFTimeInterval = 1.5, completionDelegate: CAAnimationDelegate? = nil)
    {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = Float.infinity
        if let delegate = completionDelegate
        {
            rotateAnimation.delegate = delegate
        }
        print("rotateAnimationrotateAnimationrotateAnimation")
        layer.add(rotateAnimation, forKey: nil)
    }
    
}
