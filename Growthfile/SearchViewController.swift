//
//  SearchViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 25/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol SelectedDataDelegate
{
    func selectedDataArrayFromSearchViewController(selectedDataArray:NSMutableArray)
}

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
   
    
    var isSearchEnabled = Bool()
    var dataArray = NSMutableArray()
    var delegate:SelectedDataDelegate?
    var filterDataArray = NSMutableArray()
    var selectedDataArray = NSMutableArray()
    var isSelectDataNNotMoveToProfileScreenBool = Bool()
    var isStarOrSmileySearch = Bool()
    
    @IBOutlet weak var searchTextfield: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var header: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        self.setUsersList();
        
        isSearchEnabled = false
        tableView.delegate = self
        tableView.dataSource = self
        searchTextfield.delegate = self
       
        searchView.layer.borderWidth = 1;
        searchView.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
        searchView.layer.cornerRadius = 15
        
    }

    override func viewWillAppear(_ animated: Bool)
    {
//        self.tableView.reloadData()
//        filterDataArray.removeAllObjects()
//        searchTextfield.text = ""
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if isSearchEnabled
        {
            return filterDataArray.count
        }
        return dataArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        
        cell = tableView.dequeueReusableCell(withIdentifier: "searchCellIdentifier", for: indexPath as IndexPath)
        
        let profileImageView = cell.viewWithTag(1) as! UIImageView
        profileImageView.layer.cornerRadius =  profileImageView.frame.size.height/2
        profileImageView.layer.masksToBounds = true
        var profileImageString = ""
        
        
        let profileNameFirstCharacterLabel = cell.viewWithTag(2) as! UILabel
        
        let profileName = cell.viewWithTag(3) as! UILabel
        let designation = cell.viewWithTag(4) as! UILabel
        if isSearchEnabled
        {
            let model = filterDataArray.object(at: indexPath.row) as! SearchDataModel
            profileName.text = String(model.name)!
            designation.text = String(model.designation)!
            profileImageString = String(model.profileImage)!
        }
        else
        {
            let model = dataArray.object(at: indexPath.row) as! SearchDataModel
            profileName.text = String(model.name)!
            designation.text = String(model.designation)!
            profileImageString = String(model.profileImage)!
        }
        
        commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileNameFirstCharacterLabel, profileImageview: profileImageView, imageString: profileImageString, nameString: profileName.text!)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if !isSelectDataNNotMoveToProfileScreenBool
        {
            let profileViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            if isSearchEnabled {
                let selectedItemObj = filterDataArray.object(at: indexPath.row) as! SearchDataModel
                profileViewControllerObj?.userId = selectedItemObj.id
            } else {
                let selectedItemObj = dataArray.object(at: indexPath.row) as! SearchDataModel
                profileViewControllerObj?.userId = selectedItemObj.id
            }
            
            self.navigationController?.pushViewController(profileViewControllerObj!, animated: true)
            return
        }
        
        let cell = tableView.cellForRow(at: indexPath as IndexPath)
        let selectedCellImageView = cell?.viewWithTag(5) as! UIImageView
        selectedCellImageView.isHidden = false
        
        if isSearchEnabled {
            let selectedItemObj = filterDataArray.object(at: indexPath.row) as! SearchDataModel
            selectedDataArray.add(selectedItemObj)
        } else {
            let selectedItemObj = dataArray.object(at: indexPath.row) as! SearchDataModel
            selectedDataArray.add(selectedItemObj)
        }
        
        self.delegate?.selectedDataArrayFromSearchViewController(selectedDataArray: selectedDataArray)
        _ = self.navigationController?.popViewController(animated: true)
    }
   
    internal func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath as IndexPath)
        let selectedCellImageView = cell?.viewWithTag(5) as! UIImageView
        selectedCellImageView.isHidden = true
        let selectedItemObj = dataArray.object(at: indexPath.row) as! SearchDataModel
        selectedDataArray.remove(selectedItemObj)
        print("selectedDataArrayselectedDataArray sede\(selectedDataArray)")
    }
    
    
    
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.addTarget(self, action: #selector(SearchViewController.getFilterData(textField:)), for: UIControlEvents.editingChanged)
    }
    
    func getFilterData(textField: UITextField)
    {
        if(textField.text?.characters.count == 0)
        {
            isSearchEnabled = false;
            textField.resignFirstResponder()
            tableView.reloadData()
            return
        }
        
        isSearchEnabled = true;
        let resultPredicate = NSPredicate(format: "name contains[c] %@", textField.text!)
        filterDataArray = NSMutableArray(array: dataArray.filtered(using: resultPredicate))
        print("****")
        print(filterDataArray)
        tableView.reloadData()
    }
    
    @IBAction func crossButtonAction(_ sender: Any)
    {
        isSearchEnabled = false;
        searchTextfield.text = ""
        searchTextfield.resignFirstResponder()
        tableView.reloadData()
    }
    
    @IBAction func backAction(_ sender: Any)
    {
      _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    
    public func setUsersList()
    {
       self.loader.isHidden = false
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        print(token)
        if token == "" {
            // show error message
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        
        Alamofire.request("\(Constants.domain)get/list/employees", method: .get, headers: headers)
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["response"]["data"] != JSON.null
                        {
                            let usersList = json["response"]["data"]
                            
                            if self.isStarOrSmileySearch == true {
                                for (_,user):(String, JSON) in usersList {
                                    if defaultVal.string(forKey: Constants.loggedInUserIdKey) != user["id"].stringValue {
                                        let model = SearchDataModel()
                                        model.id = Int32(user["id"].stringValue)!
                                        model.name = user["name"].stringValue
                                        model.designation = user["designation"].stringValue
                                        model.profileImage = user["profile_image"].stringValue
                                        
                                        self.dataArray.add(model)
                                    }
                                }
                            } else {
                                for (_,user):(String, JSON) in usersList {
                                    let model = SearchDataModel()
                                    model.id = Int32(user["id"].stringValue)!
                                    model.name = user["name"].stringValue
                                    model.designation = user["designation"].stringValue
                                    model.profileImage = user["profile_image"].stringValue
                                    
                                    self.dataArray.add(model)
                                }
                            }
                            
                            self.tableView.reloadData()
                        } else if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            self.showAlertMessage(msg: message)
                        }
                    }
                   self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Invalid credentials")
                    return
                }
        }
        
    }
    
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
