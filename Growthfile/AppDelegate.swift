//
//  AppDelegate.swift
//  Growthfile
//
//  Created by Preeti Sharma on 21/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData
import Firebase
import UserNotifications

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    var token = String()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyCc5tv-d60aKWdU8-qXMFIzZSwtu7RGjRE")
        
        // [START register_for_notifications]
        
        let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert,.badge,.sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        
        // [END register_for_notifications]
        FIRApp.configure()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.firInstanceIDTokenRefresh,
                                               object: nil)
     
        if defaultVal.bool(forKey: Constants.authUserKey)
        {
            let homeViewControllerObj = self.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            let navigationController = application.windows[0].rootViewController as! UINavigationController
            homeViewControllerObj?.pressBackButton = false
            navigationController.pushViewController(homeViewControllerObj!, animated: false)
        } 
        return true
    }

    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }

    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            defaultVal.set(refreshedToken, forKey: Constants.userFCMTokenKey)
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print message ID.
        print(userInfo)
        
        if let messageID = userInfo["gcmMessageIDKey"] {
            print("Message connectToFcmID: \(messageID)")
        }
        
        let notifType = userInfo["gcm.notification.notifType"]
        if notifType != nil {
            
            if let homeVC = self.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
                let navigationController = application.windows[0].rootViewController as! UINavigationController
                homeVC.pressBackButton = false
                homeVC.isCalledThroughNotification = true
                
                navigationController.pushViewController(homeVC, animated: false)
            }
        }
        
        
        // Print full message.
        print(userInfo)
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
        if(application.applicationState == UIApplicationState.inactive)
        {
            let notifType = userInfo["gcm.notification.notifType"]
            if notifType != nil {

                if let homeVC = self.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
                    let navigationController = application.windows[0].rootViewController as! UINavigationController
                    homeVC.pressBackButton = false
                    homeVC.isCalledThroughNotification = true
                    
                    navigationController.pushViewController(homeVC, animated: false)
                }
            }
            
            print("Inactive")
            //Show the view with the content of the push
            completionHandler(.newData)
            
        }else if (application.applicationState == UIApplicationState.background){
            
            let notifType = userInfo["gcm.notification.notifType"]
            if notifType != nil && ("\(notifType!)" == "card" || "\(notifType!)" == "feed" || "\(notifType!)" == "profile") {
                
                if let homeVC = self.mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
                    let navigationController = application.windows[0].rootViewController as! UINavigationController
                    homeVC.pressBackButton = false
                    homeVC.isCalledThroughNotification = true
                    
                    navigationController.pushViewController(homeVC, animated: false)
                }
            }
            
            print("Background")
            //Refresh the local model
            completionHandler(.newData)
            
        }else{
            
            
            print("Active")
            //Show an in-app banner
            completionHandler(.newData)
        }
        
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        connectToFcm()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // core data start
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.cadiridris.coreDataTemplate" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "Model", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    // Core data end
    
    public func checkUserToken() -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        do {
            let data = try self.managedObjectContext.fetch(request) as! [User]
            
            if data.count != 0 {
                self.token = data[0].token!
            }
            return true
            
        }catch let error as NSError {
            print("Unable to fetch \(error)")
        }
        
        return false
    }
    
}

