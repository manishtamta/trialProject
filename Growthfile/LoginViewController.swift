//
//  LoginViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 21/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController,UITextFieldDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
   
   
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        //Show keyboard when view did appear
        mobileNumberTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        //Hide keyboard when view did disappear
        mobileNumberTextField.resignFirstResponder()
    }

    
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        //Set maximum limit for mobileNumberTextField to 10
        guard let text = textField.text else { return true }
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 10
    }
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        return
    }
 
    @IBAction func loginToOtpAction(sender: AnyObject)
    {
        //Check mobile number is valid
        if mobileNumberTextField.text?.characters.count == 0 || (mobileNumberTextField.text?.characters.count)! < 10
        {
            self.showAlertMessage(msg: "Please enter valid mobile number")
            return
        }
        
        generateOTP(mobile: mobileNumberTextField.text!)
    }
    
    public func generateOTP(mobile: String)
    {
       loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        let params = [
            "mobile": mobile
        ]
        
        Alamofire.request("\(Constants.domain)employees/generateotp", method: .patch, parameters: params)
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["response"]["otp"] != JSON.null
                        {
                            self.continueToNextScreen()
                        }
                        else if json["msg"] != JSON.null {
                            self.showAlertMessage(msg: json["msg"].stringValue)
                        }
                    }
                self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    if error._code == -1009 || error._code == -4 {
                        self.showAlertMessage(msg: error.localizedDescription)
                    } else {
                        self.showAlertMessage(msg: "Ops... Something went wrong")
                    }
                    return
                }
        }
        
    }
    
    @IBAction func clickHereToKnowWhyButtonAction(_ sender: UIButton) {
        
        self.showAlertMessage(msg: "Growthfile is restricted only to members of teams that have singed up. To sign up visit https://growthfile.com For any issue email to: help@growthfile.com")
        
    }
    
    
    public func continueToNextScreen() {
        //
        let otpViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as? OtpViewController
        otpViewControllerObj!.mobileNumber = self.mobileNumberTextField.text!
        self.navigationController?.pushViewController(otpViewControllerObj!, animated: false)
    }
}
