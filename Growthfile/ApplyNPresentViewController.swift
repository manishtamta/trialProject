//
//  ApplyNPresentViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 28/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApplyNPresentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()

    
    var endDate = Date()
    var startDate = Date()
    var headerTitle = String()
    var endDateLabel = UILabel()
    var selectedButtontag = Int()
    var startDateLabel = UILabel()
    var commentTextview = UITextView()
    
    @IBOutlet weak var headerview: UIView!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var datePickerbackgroundView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        
        tableView.delegate = self
        tableView.dataSource = self
        headerLabel.text = headerTitle
        datePickerView.isHidden = true
        commentTextview.delegate = self
        datePickerbackgroundView.isHidden = true
        
        datePickerView.layer.cornerRadius = 7
        datePickerView.clipsToBounds = true
      
        submitView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        submitView.layer.borderWidth = 1
        submitView.layer.cornerRadius = 15
        
        //Set border color on cancelview
        cancelView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        cancelView.layer.borderWidth = 1
        cancelView.layer.cornerRadius = 15
        
        let alertBackgroundViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(ApplyNPresentViewController.hideAlertOnClick(sender:)))
        datePickerbackgroundView.addGestureRecognizer(alertBackgroundViewTapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
       
        startDateLabel.text = commonFunctionFileObj.dateToString(date: NSDate() as NSDate)
        endDateLabel.text = startDateLabel.text
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.dateFormat = "dd MMM yyyy"
        let dateVal = dateFormatter.date(from: startDateLabel.text!)

        endDate = dateVal!
        startDate = endDate
        
        if headerTitle == "Present" {
            let minDate = Calendar.current.date(byAdding: .day, value: -183, to: startDate as Date)
            datePicker.minimumDate = minDate
            
            datePicker.maximumDate = Date()
        } else
        {
            let minDate = Calendar.current.date(byAdding: .day, value: -183, to: startDate as Date)
            datePicker.minimumDate = minDate
            
            let maxDate = Calendar.current.date(byAdding: .day, value: 183, to: startDate as Date)
            datePicker.maximumDate = maxDate
        }
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(UIScreen.main.bounds.size.height <= 568.0)
        {
            self.tableView.contentOffset = CGPoint(x: 0, y: 20)
            self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height+20)
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        if(UIScreen.main.bounds.size.height <= 568.0)
        {
            self.tableView.contentOffset = CGPoint(x: 0, y: 0)
            self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height)
        }
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        if indexPath.row == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "startDateIdentifier", for: indexPath as IndexPath)
            startDateLabel = cell.viewWithTag(1) as! UILabel
            
            let showDatepickerviewButton = cell.viewWithTag(2) as! UIButton
            showDatepickerviewButton.addTarget(self, action: #selector(ApplyNPresentViewController.showDatepickerviewButtonAction(sender:)), for: UIControlEvents.touchUpInside)
            
        }
        else if indexPath.row == 1
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "lastDateIdentifier", for: indexPath as IndexPath)
            endDateLabel = cell.viewWithTag(1) as! UILabel
            
            let showDatepickerviewButton = cell.viewWithTag(2) as! UIButton
            showDatepickerviewButton.tag = 200
            showDatepickerviewButton.addTarget(self, action: #selector(ApplyNPresentViewController.showDatepickerviewButtonAction(sender:)), for: UIControlEvents.touchUpInside)
        }
        else if indexPath.row == 2
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "commentIdentifier", for: indexPath as IndexPath)
            
            commentTextview = cell.viewWithTag(1) as! UITextView
            commentTextview.layer.cornerRadius = 5;
            commentTextview.layer.borderWidth = 1
            commentTextview.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 || indexPath.row == 1
        {
            return 57
        }
        else
        {
            return 149
        }
        
    }
    
    
    
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == "Write a comment..."
        {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty
        {
            textView.text = "Write a comment..."
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.endEditing(true)
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return numberOfChars <= 200
        
    }
    
   
    
    
    func hideAlertOnClick(sender:UITapGestureRecognizer)
    {
        datePickerView.isHidden = true
        datePickerbackgroundView.isHidden = true
    }
    
    func showDatepickerviewButtonAction(sender:UIButton)
    {
        selectedButtontag = sender.tag
        datePickerbackgroundView.isHidden = false
        datePickerView.isHidden = false
        self.datePickerbackgroundView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.9)
    }
    
    
    
    @IBAction func doneButtonActionOnDatepicker(_ sender: Any)
    {
        if selectedButtontag == 200
        {
            endDate = datePicker.date
            endDateLabel.text = commonFunctionFileObj.dateToString(date: datePicker.date as NSDate)
        }
        else
        {
            startDate = datePicker.date
            startDateLabel.text = commonFunctionFileObj.dateToString(date: datePicker.date as NSDate)
            endDate = datePicker.date
            endDateLabel.text = commonFunctionFileObj.dateToString(date: datePicker.date as NSDate)
        }
        
        datePickerView.isHidden = true
        datePickerbackgroundView.isHidden = true
        
        let order = Calendar.current.compare(startDate as Date, to: endDate as Date, toGranularity: .day)
        print("orderorderorder \(order)")
    }
    
    @IBAction func submitAction(_ sender: Any)
    {
        if startDate > endDate
        {
            print(startDate)
            print(endDate)
            showAlertMessage(msg: "Start date is greater than last date")
        }
        else
        {
            self.applyLeaveApplication()
        }
        
    }
    
    @IBAction func clearAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    func applyLeaveApplication()
    {
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        var params: Parameters = [
            "from_date": commonFunctionFileObj.dateToSupportedFormatString(date: startDate as NSDate),
            "to_date": commonFunctionFileObj.dateToSupportedFormatString(date: endDate as NSDate),
            ]
        
        if self.headerTitle == "Present" {
            params["leave_type"] = "PM"
        } else {
            params["leave_type"] = "LV"
        }
        
        
        let comment = self.commentTextview.text
        if comment != "Write a comment..." {
            params["remark"] = comment
        }
        
        
        Alamofire.request("\(Constants.domain)employees/applyleave", method: .post, parameters: params, headers: headers).validate(statusCode: 200..<450)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            if response.response?.statusCode == 200 {
                                self.showSucessAlertMessage(msg: message)
                            }
                            else {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                   self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Ops... Something went wrong")
                    return
                }
        }

    }
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSucessAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: clearAction(_:)))
        self.present(alert, animated: true, completion: nil)
    }
}
