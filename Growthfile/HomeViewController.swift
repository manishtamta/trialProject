//
//  HomeViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 22/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SelectedDataDelegate,CLLocationManagerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource
{
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()

    var lat = Double()
    var lng = Double()
    var mapView = GMSMapView()
    var pressBackButton = Bool()
    var collectionView :UICollectionView!
    var locationManager = CLLocationManager()
    
    var starNameArray = NSMutableArray()
    var collectionViewHeight = Int()
    
    var refreshControl:UIRefreshControl!
    
    @IBOutlet weak var cardTableview: UITableView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var otherView: UIView!
    
    var feedData: JSON = []
    var cardId = Int()
    
    var isCalledThroughNotification = Bool()
    var cardTapForOtherView = UITapGestureRecognizer()
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        if defaultVal.bool(forKey: Constants.authUserKey) == false
        {
            self.goToLoginScreen()
        }
        else {
//            if isCalledThroughNotification == true && cardId > 0 {
//                self.showNotificationCard()
//            }else {
//                self.setUpFeedData()
//            }
            self.setUpFeedData()
        }
        
    }
    
    override func viewDidLoad()
    {
        refreshControl = UIRefreshControl()
        
        if #available(iOS 10.0, *) {
            self.cardTableview.refreshControl = refreshControl
        } else {
            self.cardTableview.addSubview(refreshControl)
        }
//        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.backgroundColor = UIColor.white
        refreshControl.tintColor = UIColor.black
        refreshControl.addTarget(self, action: #selector(HomeViewController.handleRefresh(sender:)), for: .valueChanged)
        
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        cardTableview.delegate = self
        cardTableview.dataSource = self
        otherView.isHidden = true
        
        cardTableview.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
        }
        else
        {
            print("Location services are not enabled");
        }

        starNameArray = []
        collectionViewHeight = 0
        
    }
    
    // Hide other view
    func hideOtherView() {
        otherView.isHidden = true
        self.cardTableview.removeGestureRecognizer(cardTapForOtherView)
    }

    override func viewDidDisappear(_ animated: Bool)
    {
        otherView.isHidden = true
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView.tag == 1000
        {
            return (feedData.array?.count)!
        }
        return 6
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        if tableView.tag == 1000
        {
            let card = feedData[indexPath.row]
            
            if card["table"] == "smileys"
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "smileCardCellIdentifier", for: indexPath as IndexPath)
                
                let image = cell.viewWithTag(1) as! UIImageView
                image.layer.cornerRadius = (image.frame.size.height)/2
                image.layer.masksToBounds = true
                
                let nameLabel = cell.viewWithTag(2) as! UILabel
                nameLabel.text = card["updater"]["name"].stringValue
                
                let profileLabel = cell.viewWithTag(99) as! UILabel
                let profileImageString = card["updater"]["profile_image"].stringValue
                commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileLabel, profileImageview: image, imageString: profileImageString, nameString: nameLabel.text!)
                
                let actionLabel = cell.viewWithTag(3) as! UILabel
                actionLabel.text = card["top_right_text"].stringValue
                
                let designationLabel = cell.viewWithTag(4) as! UILabel
                designationLabel.text = card["updater"]["designation"].stringValue
                
                let timeLabel = cell.viewWithTag(5) as! UILabel
                let timestamp = card["updated_at"].stringValue
                timeLabel.text = commonFunctionFileObj.timestampToCardTimeString(timestamp: timestamp)
                
                let fromLabel = cell.viewWithTag(7) as! UILabel
                fromLabel.text = card["meta_data"]["creator_name"].stringValue
                
                let toLabel = cell.viewWithTag(8) as! UILabel
                toLabel.text = card["meta_data"]["receiver_name"].stringValue
                
                
                // Add tap gesture to card header
                let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.gotoSelectedUserProfileScreen(sender:)))
                let cardheaderView = cell.viewWithTag(1000)! as UIView
                tap.accessibilityHint = card["updater"]["id"].stringValue
                cardheaderView.addGestureRecognizer(tap)
                cardheaderView.isUserInteractionEnabled = true

            }
            else if card["table"] == "lwd" {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "resignCardCellIdentifier", for: indexPath as IndexPath)
                
                let image = cell.viewWithTag(1) as! UIImageView
                image.layer.cornerRadius = (image.frame.size.height)/2
                image.layer.masksToBounds = true
                
                let nameLabel = cell.viewWithTag(2) as! UILabel
                nameLabel.text = card["updater"]["name"].stringValue
                
                let profileLabel = cell.viewWithTag(99) as! UILabel
                let profileImageString = card["updater"]["profile_image"].stringValue
                commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileLabel, profileImageview: image, imageString: profileImageString, nameString: nameLabel.text!)
                
                let actionLabel = cell.viewWithTag(3) as! UILabel
                actionLabel.text = card["top_right_text"].stringValue
                
                let designationLabel = cell.viewWithTag(4) as! UILabel
                designationLabel.text = card["updater"]["designation"].stringValue
                
                let timeLabel = cell.viewWithTag(5) as! UILabel
                let timestamp = card["updated_at"].stringValue
                timeLabel.text = commonFunctionFileObj.timestampToCardTimeString(timestamp: timestamp)
                
                let employeeNameLabel = cell.viewWithTag(7) as! UILabel
                employeeNameLabel.text = card["meta_data"]["emp_name"].stringValue
                
                let calendar = Calendar.current
                let lwdDate = commonFunctionFileObj.dateToNSDate(timestamp: card["relevant_date_from"].stringValue) as NSDate
                
                let dateLabel = cell.viewWithTag(8) as! UILabel
                dateLabel.text = String(format: "%02d", arguments:[calendar.component(.day, from: lwdDate as Date)])
                dateLabelAlignment(datelabel: dateLabel)
                
                let monthLabel = cell.viewWithTag(9) as! UILabel
                monthLabel.text = commonFunctionFileObj.NSDateToMonthDate(date: lwdDate)
                
                let yearLabel = cell.viewWithTag(10) as! UILabel
                yearLabel.text = String(calendar.component(.year, from: lwdDate as Date))
                
                let commentView = cell.viewWithTag(32)
                let commentTextView = cell.viewWithTag(11) as! UITextView
                
                adjustCommentviewAccordingToText(commentView: commentView!, commentTextView: commentTextView, indexPath: indexPath)

                let userActions = card["user_actions"].array
                let actionView = cell.viewWithTag(64)
                if (userActions?.count)! == 0
                {
                    actionView?.isHidden = true
                }
                else
                {
                    actionView?.isHidden = false
                    
                    if (userActions?.contains("ACCEPT"))!
                    {
                        cell.viewWithTag(16)?.isHidden = true  // Withdraw
                        actionView?.isHidden = false
                        let approveView = cell.viewWithTag(14)
                        approveView?.isHidden = false
                        approveView?.layer.cornerRadius = 15
                        approveView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        approveView?.layer.borderWidth = 1
                        
                        let acceptButton = cell.viewWithTag(25) as! CardActionButton
                        acceptButton.cardId = card["id"].stringValue
                        acceptButton.actionString = "ACCEPT"
                        acceptButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    }
                    else
                    {
                        cell.viewWithTag(14)?.isHidden = true
                    }
                    
                    if (userActions?.contains("REJECT"))! {
                        cell.viewWithTag(16)?.isHidden = true  // Withdraw
                        
                        let rejectView = cell.viewWithTag(15)
                        rejectView?.isHidden = false
                        rejectView?.layer.cornerRadius = 15
                        rejectView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        rejectView?.layer.borderWidth = 1
                        
                        let rejectButton = cell.viewWithTag(50) as! CardActionButton
                        rejectButton.cardId = card["id"].stringValue
                        rejectButton.actionString = "REJECT"
                        rejectButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    }
                    
                    if (userActions?.contains("WITHDRAW"))! {
                        
                        cell.viewWithTag(14)?.isHidden = true // Accept
                        cell.viewWithTag(15)?.isHidden = true // Reject
                        
                        let withdrawView = cell.viewWithTag(16)
                        withdrawView?.isHidden = false
                        withdrawView?.layer.cornerRadius = 15
                        withdrawView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        withdrawView?.layer.borderWidth = 1
                        
                        let withdrawButton = cell.viewWithTag(150) as! CardActionButton
                        withdrawButton.cardId = card["id"].stringValue
                        withdrawButton.actionString = "WITHDRAW"
                        withdrawButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    }
                }

                
                // Add tap gesture to card header
                let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.gotoSelectedUserProfileScreen(sender:)))
                let cardheaderView = cell.viewWithTag(1000)! as UIView
                tap.accessibilityHint = card["updater"]["id"].stringValue
                cardheaderView.addGestureRecognizer(tap)
                cardheaderView.isUserInteractionEnabled = true
                
            }
            else if card["table"] == "leave_applications" {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "leaveCardCellIdentifier", for: indexPath as IndexPath)
                
                let image = cell.viewWithTag(1) as! UIImageView
                image.layer.cornerRadius = (image.frame.size.height)/2
                image.layer.masksToBounds = true
                
                let nameLabel = cell.viewWithTag(2) as! UILabel
                nameLabel.text = card["updater"]["name"].stringValue
                
                let profileLabel = cell.viewWithTag(99) as! UILabel
                let profileImageString = card["updater"]["profile_image"].stringValue
                commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileLabel, profileImageview: image, imageString: profileImageString, nameString: nameLabel.text!)
                
                let actionLabel = cell.viewWithTag(3) as! UILabel
                actionLabel.text = card["top_right_text"].stringValue
                
                let designationLabel = cell.viewWithTag(4) as! UILabel
                designationLabel.text = card["updater"]["designation"].stringValue
                
                let cardApplicationTypeLabel = cell.viewWithTag(33) as! UILabel
                let leaveType = card["meta_data"]["leave_type"].stringValue
                if leaveType == "LV" {
                    cardApplicationTypeLabel.text = "LEAVE"
                } else if leaveType == "PM" {
                    cardApplicationTypeLabel.text = "PRESENT"
                } else {
                    cardApplicationTypeLabel.text = leaveType
                }
                
                let leaveCreatorNameLabel = cell.viewWithTag(20) as! UILabel
                leaveCreatorNameLabel.text = card["meta_data"]["emp_name"].stringValue
                
                let timeLabel = cell.viewWithTag(5) as! UILabel
                let timestamp = card["updated_at"].stringValue
                timeLabel.text = commonFunctionFileObj.timestampToCardTimeString(timestamp: timestamp)
                
                let calendar = Calendar.current
                let fromDate = commonFunctionFileObj.dateToNSDate(timestamp: card["relevant_date_from"].stringValue) as NSDate
                let startDateLabel = cell.viewWithTag(7) as! UILabel
                startDateLabel.text = String(format: "%02d", arguments:[calendar.component(.day, from: fromDate as Date)])
                dateLabelAlignment(datelabel: startDateLabel)
                
                let startMonthLabel = cell.viewWithTag(8) as! UILabel
                startMonthLabel.text = commonFunctionFileObj.NSDateToMonthDate(date: fromDate)
                
                let startYearLabel = cell.viewWithTag(9) as! UILabel
                startYearLabel.text = String(calendar.component(.year, from: fromDate as Date))

                let toDate = commonFunctionFileObj.dateToNSDate(timestamp: card["relevant_date_to"].stringValue) as NSDate
                let endDateLabel = cell.viewWithTag(10) as! UILabel
                endDateLabel.text = String(format: "%02d", arguments:[calendar.component(.day, from: toDate as Date)])
                dateLabelAlignment(datelabel: endDateLabel)
                
                let endMonthLabel = cell.viewWithTag(11) as! UILabel
                endMonthLabel.text = commonFunctionFileObj.NSDateToMonthDate(date: toDate)
                
                let endYearLabel = cell.viewWithTag(12) as! UILabel
                endYearLabel.text = String(calendar.component(.year, from: toDate as Date))
                
                let commentView = cell.viewWithTag(32)
                let commentTextView = cell.viewWithTag(13) as! UITextView
                adjustCommentviewAccordingToText(commentView: commentView!, commentTextView: commentTextView, indexPath: indexPath)
            
                let userActions = card["user_actions"].array
                let actionView = cell.viewWithTag(64)
                
                if (userActions?.count)! == 0
                {
                    actionView?.isHidden = true
                } else {
                    actionView?.isHidden = false
                    
                    if (userActions?.contains("ACCEPT"))! {
                        cell.viewWithTag(16)?.isHidden = true  // Withdraw
                        
                        let approveView = cell.viewWithTag(14)
                        approveView?.isHidden = false
                        approveView?.layer.cornerRadius = 15
                        approveView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        approveView?.layer.borderWidth = 1
                        
                        let acceptButton = cell.viewWithTag(25) as! CardActionButton
                        acceptButton.cardId = card["id"].stringValue
                        acceptButton.actionString = "ACCEPT"
                        acceptButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    } else {
                        cell.viewWithTag(14)?.isHidden = true
                    }
                    
                    if (userActions?.contains("REJECT"))! {
                        cell.viewWithTag(16)?.isHidden = true  // Withdraw
                        
                        let rejectView = cell.viewWithTag(15)
                        rejectView?.isHidden = false
                        rejectView?.layer.cornerRadius = 15
                        rejectView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        rejectView?.layer.borderWidth = 1
                        
                        let rejectButton = cell.viewWithTag(50) as! CardActionButton
                        rejectButton.cardId = card["id"].stringValue
                        rejectButton.actionString = "REJECT"
                        rejectButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    }
                   
                    
                    if (userActions?.contains("WITHDRAW"))! {
                        
                        cell.viewWithTag(14)?.isHidden = true // Accept
                        cell.viewWithTag(15)?.isHidden = true // Reject
                        
                        let withdrawView = cell.viewWithTag(16)
                        withdrawView?.isHidden = false
                        withdrawView?.layer.cornerRadius = 15
                        withdrawView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        withdrawView?.layer.borderWidth = 1
                        
                        let withdrawButton = cell.viewWithTag(150) as! CardActionButton
                        withdrawButton.cardId = card["id"].stringValue
                        withdrawButton.actionString = "WITHDRAW"
                        withdrawButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    }
                    
                }
                
                
                // Add tap gesture to card header
                let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.gotoSelectedUserProfileScreen(sender:)))
                let cardheaderView = cell.viewWithTag(1000)! as UIView
                tap.accessibilityHint = card["updater"]["id"].stringValue
                cardheaderView.addGestureRecognizer(tap)
                cardheaderView.isUserInteractionEnabled = true
                
            }
            else if card["table"] == "transactions"
            {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "transactionCardCellIdentifier", for: indexPath as IndexPath)
                
                let image = cell.viewWithTag(1) as! UIImageView
                image.layer.cornerRadius = (image.frame.size.height)/2
                image.layer.masksToBounds = true
                
                let nameLabel = cell.viewWithTag(2) as! UILabel
                nameLabel.text = card["updater"]["name"].stringValue
                
                let profileLabel = cell.viewWithTag(99) as! UILabel
                let profileImageString = card["updater"]["profile_image"].stringValue
                commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileLabel, profileImageview: image, imageString: profileImageString, nameString: nameLabel.text!)
                
                let actionLabel = cell.viewWithTag(3) as! UILabel
                actionLabel.text = card["top_right_text"].stringValue
                
                let designationLabel = cell.viewWithTag(4) as! UILabel
                designationLabel.text = card["updater"]["designation"].stringValue
                
                let timeLabel = cell.viewWithTag(5) as! UILabel
                let timestamp = card["updated_at"].stringValue
                timeLabel.text = commonFunctionFileObj.timestampToCardTimeString(timestamp: timestamp)
                
                let collectionLabel = cell.viewWithTag(6) as! UILabel
                let amount = card["meta_data"]["amount"].intValue
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                collectionLabel.text = "₹\(numberFormatter.string(from: NSNumber(value: amount))!)"
                
                
                let paymentTypeLabel = cell.viewWithTag(111) as! UILabel
                let status = card["meta_data"]["status"].stringValue
                if status == "EXPENSE" {
                    paymentTypeLabel.text = "Payment"
                } else if status == "REVENUE" {
                    paymentTypeLabel.text = "Collection"
                }
                
                let dueDate = commonFunctionFileObj.dateToNSDate(timestamp: card["relevant_date_from"].stringValue) as NSDate
                let calendar = Calendar.current
                
                let dueDateLabel = cell.viewWithTag(7) as! UILabel
                dueDateLabel.text = String(format: "%02d", arguments:[calendar.component(.day, from: dueDate as Date)])
                dateLabelAlignment(datelabel: dueDateLabel)
                
                let dueMonthLabel = cell.viewWithTag(8) as! UILabel
                dueMonthLabel.text = commonFunctionFileObj.NSDateToMonthDate(date: dueDate)
                
                let dueYearLabel = cell.viewWithTag(9) as! UILabel
                dueYearLabel.text = String(calendar.component(.year, from: dueDate as Date))
                
                let commentView = cell.viewWithTag(32)
                let commentTextView = cell.viewWithTag(11) as! UITextView
                let companyNameLabel = cell.viewWithTag(10) as! UILabel
                adjustLocNTransactionCommentviewAccordingToText(commentView: commentView!, commentTextView: commentTextView, companyNameLabel: companyNameLabel, indexPath: indexPath)
            
                let userActions = card["user_actions"].array
                let actionView = cell.viewWithTag(64)
                if (userActions?.count)! == 0 {
                    actionView?.isHidden = true
                } else {
                    actionView?.isHidden = false
                    
                    if (userActions?.contains("EDIT"))! {
                        let editView = cell.viewWithTag(12)
                        editView?.isHidden = false
                        editView?.layer.cornerRadius = 15
                        editView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        editView?.layer.borderWidth = 1
                        
                        let editButton = cell.viewWithTag(25) as! CardActionButton
                        editButton.cardId = card["id"].stringValue
                        editButton.actionString = "EDIT"
                        editButton.selectedCellIndex = indexPath.row

                        editButton.addTarget(self, action: #selector(HomeViewController.editTransaction(sender:)), for: .touchUpInside)
                    } else {
                        cell.viewWithTag(12)?.isHidden = true
                    }
                    
                    if (userActions?.contains("WITHDRAW"))! {
                        let withdrawView = cell.viewWithTag(13)
                        withdrawView?.isHidden = false
                        withdrawView?.layer.cornerRadius = 15
                        withdrawView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        withdrawView?.layer.borderWidth = 1
                        
                        let withdrawButton = cell.viewWithTag(30) as! CardActionButton
                        withdrawButton.cardId = card["id"].stringValue
                        withdrawButton.actionString = "WITHDRAW"
                        withdrawButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    } else {
                        cell.viewWithTag(13)?.isHidden = true
                    }
                }
                
                
                // Add tap gesture to card header
                let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.gotoSelectedUserProfileScreen(sender:)))
                let cardheaderView = cell.viewWithTag(1000)! as UIView
                tap.accessibilityHint = card["updater"]["id"].stringValue
                cardheaderView.addGestureRecognizer(tap)
                cardheaderView.isUserInteractionEnabled = true

            }
            else if card["table"] == "marked_locations" {
                
                cell = tableView.dequeueReusableCell(withIdentifier: "locationCardCellIdentifier", for: indexPath as IndexPath)
                
                let image = cell.viewWithTag(1) as! UIImageView
                image.layer.cornerRadius = (image.frame.size.height)/2
                image.layer.masksToBounds = true
                
                let nameLabel = cell.viewWithTag(2) as! UILabel
                nameLabel.text = card["updater"]["name"].stringValue
                
                let profileLabel = cell.viewWithTag(99) as! UILabel
                let profileImageString = card["updater"]["profile_image"].stringValue
                commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileLabel, profileImageview: image, imageString: profileImageString, nameString: nameLabel.text!)
                
                let actionLabel = cell.viewWithTag(3) as! UILabel
                actionLabel.text = card["top_right_text"].stringValue
                
                let designationLabel = cell.viewWithTag(4) as! UILabel
                designationLabel.text = card["updater"]["designation"].stringValue
                
                let timeLabel = cell.viewWithTag(5) as! UILabel
                let timestamp = card["updated_at"].stringValue
                timeLabel.text = commonFunctionFileObj.timestampToCardTimeString(timestamp: timestamp)
                
                let screenWidth = Int(UIScreen.main.bounds.size.width)
                let mapImageView = cell.viewWithTag(6) as! UIImageView
                let latString = card["meta_data"]["lat"].stringValue
                let lngString = card["meta_data"]["lng"].stringValue
                let url = NSURL(string: "https://maps.googleapis.com/maps/api/staticmap?center=\(latString),\(lngString)&zoom=17&size=\(screenWidth)x127&markers=\(latString),\(lngString)&maptype=roadmap&format=png")
                mapImageView.sd_setImage(with: url! as URL)
                
                
                let commentView = cell.viewWithTag(32)
                let commentTextView = cell.viewWithTag(8) as! UITextView
                let companyNameLabel = cell.viewWithTag(7) as! UILabel
                adjustLocNTransactionCommentviewAccordingToText(commentView: commentView!, commentTextView: commentTextView, companyNameLabel: companyNameLabel, indexPath: indexPath)

                
                let userActions = card["user_actions"].array
               
                let actionView = cell.viewWithTag(64)
                if (userActions?.count)! == 0 {
                    actionView?.isHidden = true
                } else {
                    actionView?.isHidden = false
                    
                    if (userActions?.contains("WITHDRAW"))! {
                        let withdrawView = cell.viewWithTag(9)
                        withdrawView?.isHidden = false
                        withdrawView?.layer.cornerRadius = 15
                        withdrawView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        withdrawView?.layer.borderWidth = 1
                        
                        let withdrawButton = cell.viewWithTag(128) as! CardActionButton
                        withdrawButton.cardId = card["id"].stringValue
                        withdrawButton.actionString = "WITHDRAW"
                        withdrawButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    }
                }
                
                
                // Add tap gesture to card header
                let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.gotoSelectedUserProfileScreen(sender:)))
                let cardheaderView = cell.viewWithTag(1000)! as UIView
                tap.accessibilityHint = card["updater"]["id"].stringValue
                cardheaderView.addGestureRecognizer(tap)
                cardheaderView.isUserInteractionEnabled = true

            }
            
            else if card["table"] == "stars"
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "starCardCellIdentifier", for: indexPath as IndexPath)
                
                let image = cell.viewWithTag(1) as! UIImageView
                image.layer.cornerRadius = (image.frame.size.height)/2
                image.layer.masksToBounds = true
                
                let nameLabel = cell.viewWithTag(2) as! UILabel
                nameLabel.text = card["updater"]["name"].stringValue
                
                let size: CGSize = nameLabel.text!.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
                nameLabel.frame = CGRect(x: 68, y: 8, width: size.width, height: 21)
                nameLabel.translatesAutoresizingMaskIntoConstraints = true
                
                let supporterLabel = cell.viewWithTag(788) as! UILabel
                let supportersCount = card["star"]["supporters"].array?.count
                if supportersCount != nil {
                    supporterLabel.text = "+\(supportersCount!)"
                } else {
                    supporterLabel.text = ""
                }
                
                let profileLabel = cell.viewWithTag(99) as! UILabel
                let profileImageString = card["updater"]["profile_image"].stringValue
                commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileLabel, profileImageview: image, imageString: profileImageString, nameString: nameLabel.text!)
                
                let actionLabel = cell.viewWithTag(3) as! UILabel
                actionLabel.text = card["top_right_text"].stringValue
                
                let designationLabel = cell.viewWithTag(4) as! UILabel
                designationLabel.text = card["updater"]["designation"].stringValue
                
                let timeLabel = cell.viewWithTag(5) as! UILabel
                let timestamp = card["updated_at"].stringValue
                timeLabel.text = commonFunctionFileObj.timestampToCardTimeString(timestamp: timestamp)
                
                let fromLabel = cell.viewWithTag(7) as! UILabel
                fromLabel.text = card["meta_data"]["creator_name"].stringValue
                
                
                let toLabel = cell.viewWithTag(8) as! UILabel
                var names = ""
                let receivers = card["receivers"]
                for (_,receiver):(String, JSON) in receivers {
                    names = names + receiver["name"].stringValue + "\n"
                    starNameArray.add(receiver["name"].stringValue)
                }
                
                toLabel.text = names
                let commentView = cell.viewWithTag(32)
                let commentTextView = cell.viewWithTag(9) as! UITextView
                adjustCommentviewAccordingToText(commentView: commentView!, commentTextView: commentTextView, indexPath: indexPath)

                let userActions = card["user_actions"].array
                let actionView = cell.viewWithTag(64)
                if (userActions?.count)! == 0
                {
                    actionView?.isHidden = true
                }
                else
                {
                    actionView?.isHidden = false
                    
                    if (userActions?.contains("SUPPORT"))! == true
                    {
                        let supportView = cell.viewWithTag(15)
                        supportView?.isHidden = false
                        supportView?.layer.cornerRadius = 15
                        supportView?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
                        supportView?.layer.borderWidth = 1
                        
                        let supportButton = cell.viewWithTag(25) as! CardActionButton
                        supportButton.cardId = card["id"].stringValue
                        supportButton.actionString = "SUPPORT"
                        supportButton.addTarget(self, action: #selector(HomeViewController.actionButtonClicked(sender:)), for: .touchUpInside)
                    }
                }
                

                collectionView = cell.viewWithTag(11) as! UICollectionView
                let toTextLabel = cell.viewWithTag(10) as! UILabel
                let toLabelViewContainer = cell.viewWithTag(12)
                if starNameArray.count > 3
                {
                     toTextLabel.frame = CGRect(x:CGFloat((toLabelViewContainer!.bounds.size.width - toTextLabel.frame.size.width)/2)  , y: CGFloat(10) , width: toTextLabel.frame.size.width, height: toTextLabel.frame.size.height)
                    toTextLabel.translatesAutoresizingMaskIntoConstraints = true
                    self.collectionView.frame = CGRect(x: Int((toLabelViewContainer!.bounds.size.width - collectionView.frame.size.width)/2),y: 30,width: Int(collectionView.frame.size.width),height: collectionViewHeight)
                    collectionView.translatesAutoresizingMaskIntoConstraints = true
                }
                else
                {
                    toTextLabel.frame = CGRect(x:CGFloat((toLabelViewContainer!.bounds.size.width - toTextLabel.frame.size.width)/2)  , y: CGFloat(37) , width: toTextLabel.frame.size.width, height: toTextLabel.frame.size.height)
                    toTextLabel.textAlignment = NSTextAlignment.center
                    toTextLabel.translatesAutoresizingMaskIntoConstraints = false
                    self.collectionView.frame = CGRect(x: Int((toLabelViewContainer!.bounds.size.width - collectionView.frame.size.width)/2),y: 57,width: Int(collectionView.frame.size.width),height: collectionViewHeight)
                    collectionView.translatesAutoresizingMaskIntoConstraints = true
                }
                
                for _ in starNameArray
                {
                    collectionViewHeight = collectionViewHeight + 14
                    collectionView.reloadData()
                }
                
                collectionViewHeight = 0
                starNameArray = []
                
                
                // Add tap gesture to card header
                let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.gotoSelectedUserProfileScreen(sender:)))
                let cardheaderView = cell.viewWithTag(1000)! as UIView
                tap.accessibilityHint = card["updater"]["id"].stringValue
                cardheaderView.addGestureRecognizer(tap)
                cardheaderView.isUserInteractionEnabled = true
                
            }
        }
        else
        {
            if indexPath.row == 0
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "growthfileIconIdentifier", for: indexPath as IndexPath)
            }
            else if indexPath.row == 1
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "instantReportIdentifier", for: indexPath as IndexPath)
            }
            else if indexPath.row == 2
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "meetingIdentifier", for: indexPath as IndexPath)
                
                let view = cell.viewWithTag(1)
                view?.layer.cornerRadius = (view?.frame.size.height)!/2
                view?.layer.masksToBounds = true
                view?.layer.borderColor = UIColor.darkGray.cgColor
                view?.layer.borderWidth = 1.0
                
            }
            else if indexPath.row == 3
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "invoiceIdentifier", for: indexPath as IndexPath)
                
                let view = cell.viewWithTag(1)
                view?.layer.cornerRadius = (view?.frame.size.height)!/2
                view?.layer.masksToBounds = true
                view?.layer.borderColor = UIColor.darkGray.cgColor
                view?.layer.borderWidth = 1.0
                
                let label = cell.viewWithTag(3) as! UILabel
                let myString: NSString = "amount just click on"
                let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)])
                label.frame = CGRect(x: 36,y:  27,width: size.width,height: size.height)
                
                label.translatesAutoresizingMaskIntoConstraints = true
                
                
            }
            else if indexPath.row == 4
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "thanksIdentifier", for: indexPath as IndexPath)
                
                let view = cell.viewWithTag(1)
                view?.layer.cornerRadius = (view?.frame.size.height)!/2
                view?.layer.masksToBounds = true
                view?.layer.borderColor = UIColor.darkGray.cgColor
                view?.layer.borderWidth = 1.0
                
                let label = cell.viewWithTag(2) as! UILabel
                let myString: NSString = "Say thank you or just a"
                let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)])
                label.frame = CGRect(x: 36,y:  10,width: size.width,height: size.height)
                
                label.translatesAutoresizingMaskIntoConstraints = true
                
            }
            else if indexPath.row == 5
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "congratsIdentifier", for: indexPath as IndexPath)
                
                let view = cell.viewWithTag(1)
                view?.layer.cornerRadius = (view?.frame.size.height)!/2
                view?.layer.masksToBounds = true
                view?.layer.borderColor = UIColor.darkGray.cgColor
                view?.layer.borderWidth = 1.0
                
                let label = cell.viewWithTag(2) as! UILabel
                let myString: NSString = "Congratulate a good job with a"
                let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)])
                label.frame = CGRect(x: 36,y:  10,width: size.width,height: size.height)
                
                label.translatesAutoresizingMaskIntoConstraints = true
            }
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView.tag == 1000
        {
            let card = feedData[indexPath.row]
            
            if card["table"] == "smileys"
            {
                return 187
            }
            else if card["table"] == "lwd"
            {
               return heightOfCardCell(indexPath: indexPath)
            }
            else if card["table"] == "leave_applications"
            {
                return heightOfCardCell(indexPath: indexPath)
            }
            else if card["table"] == "transactions"
            {
                return heightOfLocNTranscCardCell(indexPath: indexPath)
            }
            else if card["table"] == "marked_locations"
            {
               return heightOfLocNTranscCardCell(indexPath: indexPath)
            }
            else if card["table"] == "stars"
            {
                return heightOfCardCell(indexPath: indexPath)
            }
            else
            {
                return 347
            }
        }
        else
        {
            if indexPath.row == 0
            {
                return 152
            }
            else if indexPath.row == 1
            {
                return 63
            }
            else if indexPath.row == 4 ||  indexPath.row == 5
            {
                return 47
            }
            else
            {
                return 60
            }
        }
      
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("click click click!!!!")
        if otherView.isHidden == false{
            otherView.isHidden = true
        }
        
        if tableView.tag == 1000
        {
            let card = feedData[indexPath.row]
            if card["table"] == "marked_locations"
            {
                let url = URL(string:"https://maps.google.com/?q=@\(card["meta_data"]["lat"]),\(card["meta_data"]["lng"])")!
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
   
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return starNameArray.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nameCellIdentifier", for: indexPath as IndexPath)
        
        let nameLabel = cell.viewWithTag(1) as! UILabel
        nameLabel.text = String(describing: self.starNameArray[indexPath.item])
        return cell
    }
    

    
    
    
    func selectedDataArrayFromSearchViewController(selectedDataArray: NSMutableArray)
    {
        
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        locationManager.stopUpdatingLocation()
    }
    

    @IBAction func backbuttonAction(_ sender: Any)
    {
        if pressBackButton
        {
            pressBackButton = false
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func showLocationViewController(_ sender: Any)
    {
        let locationViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as? LocationViewController
        self.navigationController?.pushViewController(locationViewControllerObj!, animated: false)
    }
    
    @IBAction func showTransactionViewController(_ sender: Any)
    {
        let transactionViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as? TransactionViewController
        self.navigationController?.pushViewController(transactionViewControllerObj!, animated: false)
    }
    
    @IBAction func showStarViewControler(_ sender: Any)
    {
        let starViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "StarViewController") as? StarViewController
        self.navigationController?.pushViewController(starViewControllerObj!, animated: false)
    }
   
    @IBAction func showSmileViewController(_ sender: Any)
    {
        let smileViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SmileViewController") as? SmileViewController
        self.navigationController?.pushViewController(smileViewControllerObj!, animated: false)
    }
    
    @IBAction func moreViewAction(_ sender: Any)
    {
        otherView.isHidden = false
        cardTapForOtherView = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.hideOtherView))
        cardTapForOtherView.numberOfTapsRequired = 1
        self.cardTableview.addGestureRecognizer(cardTapForOtherView)
    }
    
    @IBAction func showPresentViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let applyNPresentViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ApplyNPresentViewController") as? ApplyNPresentViewController
        applyNPresentViewControllerObj?.headerTitle = "Present"
        self.navigationController?.pushViewController(applyNPresentViewControllerObj!, animated: false)
    }
   
    @IBAction func ShowLeaveViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let applyNPresentViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ApplyNPresentViewController") as? ApplyNPresentViewController
        applyNPresentViewControllerObj?.headerTitle = "Leave"
        self.navigationController?.pushViewController(applyNPresentViewControllerObj!, animated: false)
    }
   
    @IBAction func ShowResignViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let resignViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ResignViewController") as? ResignViewController
        self.navigationController?.pushViewController(resignViewControllerObj!, animated: false)
    }
    
    @IBAction func showSearchViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let searchViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
        searchViewControllerObj?.isSelectDataNNotMoveToProfileScreenBool = false
        searchViewControllerObj!.delegate = self
        self.navigationController?.pushViewController(searchViewControllerObj!, animated: false)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    public func setUpFeedData()
    {
      loader.isHidden = false
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        Alamofire.request("\(Constants.domain)get/employees/cards", method: .get, headers: headers).validate(statusCode: 200..<450)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if response.response?.statusCode == 200 {
                            self.feedData = json["response"]["data"]
                            if self.feedData.count > 0 {
                                self.cardTableview.reloadData()
                                self.collectionView.reloadData()
                            } else {
                                self.gotoEmptyFeedScreen()
                            }
                            
                        } else if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                                if defaultVal.bool(forKey: Constants.authUserKey) == false
                                {
                                    self.goToLoginScreen()
                                }
                                self.showAlertMessage(msg: "You've been logged out.")
                            }
                            else if message == "Object not found." {
                                self.gotoEmptyFeedScreen()
                            }
                            else {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                    self.loader.isHidden = true
                    if self.refreshControl.isRefreshing
                    {
                        self.refreshControl.endRefreshing()
                    }
                    
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    if error._code == -1009 || error._code == -4 {
                        self.showAlertMessage(msg: error.localizedDescription)
                    } else {
                        self.showAlertMessage(msg: "Ops... Something went wrong")
                    }
                    if self.refreshControl.isRefreshing
                    {
                        self.refreshControl.endRefreshing()
                    }
                }
        }
        
    }

    @IBAction func trancAction(_ sender: Any) {
        print("jhdgfjhsdgfhjsdfgjhsdfgjhsdfgsdf")
    }
    @IBAction func transactionWithdrawaCTION(_ sender: Any)
    {
       print("transactionWithdrawaCTIONtransactionWithdrawaCTION")
    }
    @IBAction func transactionEditAction(_ sender: Any)
    {
       print("transactionWithdrawaCTIONtransactionWithdrawaCTION")
    }
    
    @IBAction func starSupportedAction(_ sender: Any) {
        print("starSupportedActionstarSupportedActionstarSupportedAction")
    }
    
    @IBAction func acceptAction(_ sender: Any)
    {
        print("acceptActionacceptActionacceptAction")
    }
   
    @IBAction func rejectAction(_ sender: Any) {
        print("rejectActionrejectActionrejectActionrejectAction")
    }
    
    
    func actionButtonClicked(sender: CardActionButton)
    {
        if sender.cardId != "" && sender.actionString != "" {
            self.followUpAction(cardId: sender.cardId!, actionString: sender.actionString!)
        }
        
//        if sender.actionString == "EDIT" {
//            sender.isEnabled = true
//        } else {
//            sender.isEnabled = false
//        }
        sender.cardId = ""
        sender.actionString = ""
    }
    
    func editTransaction(sender: CardActionButton)
    {
        if sender.cardId != "" && sender.actionString != "" {
            let transactionViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as? TransactionViewController
            transactionViewControllerObj?.cardId = sender.cardId!

            transactionViewControllerObj?.editAmount = feedData[sender.selectedCellIndex]["meta_data"]["amount"].stringValue
            transactionViewControllerObj?.editDueDate = feedData[sender.selectedCellIndex]["relevant_date_from"].stringValue
            transactionViewControllerObj?.editPaymentType = feedData[sender.selectedCellIndex]["meta_data"]["status"].stringValue
            transactionViewControllerObj?.editAccountName = feedData[sender.selectedCellIndex]["meta_data"]["account_name"].stringValue
            transactionViewControllerObj?.editComment = feedData[sender.selectedCellIndex]["meta_data"]["comment"].stringValue

            self.navigationController?.pushViewController(transactionViewControllerObj!, animated: false)
        }
        sender.cardId = ""
        sender.actionString = ""
    }
    
    func followUpAction(cardId: String, actionString: String)
    {
        loader.isHidden = false
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        let params: Parameters = [
            "card_id": cardId,
            "action": actionString
        ]
        
        Alamofire.request("\(Constants.domain)employees/followupaction", method: .post, parameters: params, headers: headers).validate(statusCode: 200..<450)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value
                    {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if response.response?.statusCode == 200 {
                            self.showSuccessAlertMessage(msg: json["msg"].stringValue)
                        } else if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            self.showAlertMessage(msg: message)
                        }
                    }
                    self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Ops... Something went wrong")
                    return
                }
        }
    }
    
    
    func showNotificationCard() {
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        if cardId > 0 {
            let params = ["card_id":cardId]
            print(headers)
            Alamofire.request("\(Constants.domain)get/employees/carddetails", method: .post, parameters: params, headers: headers).validate(statusCode: 200..<450)
                .validate(contentType: ["application/json"])
                .responseJSON { response in
                    switch response.result
                    {
                    case .success:
                        if let JSONdata = response.result.value {
                            print("JSON: \(JSONdata)")
                            let json = JSON(JSONdata)
                            if response.response?.statusCode == 200 {
                                self.feedData = json["response"]["data"]
                                self.cardTableview.reloadData()
                                self.collectionView.reloadData()
                            } else if json["msg"] != "" {
                                let message = json["msg"].stringValue;
                                if message == "Token expired" || message == "Token invalid" {
                                    defaultVal.set(false, forKey: Constants.authUserKey)
                                    if defaultVal.bool(forKey: Constants.authUserKey) == false
                                    {
                                        self.goToLoginScreen()
                                    }
                                    self.showAlertMessage(msg: "You've been logged out.")
                                }
                                else if message == "Object not found." {
                                    let emptyFeedViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "EmptyFeedViewController") as? EmptyFeedViewController
                                    self.navigationController?.pushViewController(emptyFeedViewControllerObj!, animated: false)
                                }
                                else {
                                    self.showAlertMessage(msg: message)
                                }
                            }
                        }
                        self.loader.isHidden = true
                        
                    case .failure(let error):
                        print(error)
                        self.loader.isHidden = true
                        if error._code == -1009 || error._code == -4 {
                            self.showAlertMessage(msg: error.localizedDescription)
                        } else {
                            self.showAlertMessage(msg: "Ops... Something went wrong")
                        }
                    }
            }
        }
        
    }
    
    
    // Go to selected card user profile screen
    func gotoSelectedUserProfileScreen(sender: UITapGestureRecognizer) {
        let userId = sender.accessibilityHint
        if userId != nil && userId != "" {
            let profileViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            
            profileViewControllerObj?.userId = Int32(userId!)!
            profileViewControllerObj?.isPreviousScreenHomeScreen = true
            
            self.navigationController?.pushViewController(profileViewControllerObj!, animated: true)
        }
    }
    
    
    func goToLoginScreen() {
        let loginViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        self.navigationController?.pushViewController(loginViewControllerObj!, animated: true)
    }
    
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
//        self.setUpFeedData()
    }
    
    func showSuccessAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        self.setUpFeedData()
    }
        
    func adjustCommentviewAccordingToText(commentView:UIView,commentTextView:UITextView,indexPath:IndexPath)
    {
        let card = feedData[indexPath.row]
        
        if card["update_comment"].stringValue == ""
        {
            commentView.isHidden = true
        }
        else
        {
            commentView.isHidden = false
            commentTextView.text = card["update_comment"].stringValue
        }
        
        let commentString: NSString = card["update_comment"].stringValue as NSString
        let size: CGSize = commentString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        var height = CGFloat(0)
        
        if size.width > 275
        {
            height = CGFloat(size.height) + CGFloat(20)
        }
        else
        {
            height = (CGFloat(2) * CGFloat(size.height)) + CGFloat(18)
        }
        
        commentTextView.frame = CGRect(x: 14, y: 4, width: self.view.frame.size.width - 28, height: height + CGFloat(7))
        commentTextView.translatesAutoresizingMaskIntoConstraints = true
        
        commentView.frame = CGRect(x: 0, y: 184, width: self.view.frame.size.width, height: height + CGFloat(21))
        commentView.translatesAutoresizingMaskIntoConstraints = true
        commentTextView.scrollRangeToVisible(NSMakeRange(0, 0))

    }
    
    func heightOfCardCell(indexPath:IndexPath) -> CGFloat
    {
        let card = feedData[indexPath.row]
        var heightOfCard = CGFloat(185)
        
        let commentString: NSString = card["update_comment"].stringValue as NSString
        let size: CGSize = commentString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        
        if size.width > 325
        {
            heightOfCard = heightOfCard + CGFloat(size.height) * 2 + CGFloat(18)
        }
        else if size.width >= 1
        {
            heightOfCard = heightOfCard + CGFloat(size.height) + CGFloat(18)
        }
        
        if (card["user_actions"].array?.count)! != 0
        {
            heightOfCard = heightOfCard + CGFloat(48)
        }
        
        return CGFloat(heightOfCard)
    }
    
    
    func adjustLocNTransactionCommentviewAccordingToText(commentView:UIView,commentTextView:UITextView,companyNameLabel:UILabel,indexPath:IndexPath)
    {
        let card = feedData[indexPath.row]
        if card["meta_data"]["account_name"].stringValue != "" || card["update_comment"].stringValue != ""
        {
            companyNameLabel.text = card["meta_data"]["account_name"].stringValue
            commentTextView.text = card["update_comment"].stringValue
            commentView.isHidden = false
        } else
        {
            commentView.isHidden = true
        }
        
        let myString: NSString = card["update_comment"].stringValue as NSString
        
        let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        var height = CGFloat(0)
        if size.width > 325
        {
            height = CGFloat(size.height) + CGFloat(30)
        }
        else if size.width < 325 && size.width != 0
        {
            height = (CGFloat(2) * CGFloat(size.height)) + CGFloat(30)
        }
        else
        {
            height = CGFloat(30)
        }
        
      
        
        var yAxisVal = 1
        print("jhjkhkjhkjhkjh \(card["meta_data"]["account_name"].stringValue)")
        var heightVal = 0
        if card["meta_data"]["account_name"].stringValue != ""
        {
            yAxisVal = 22
            heightVal = Int(height) - 9
        }
        else
        {
            heightVal = Int(height) + 9
        }
        commentView.frame = CGRect(x: 0, y: 184, width: self.view.frame.size.width, height: height + CGFloat(30))
        commentView.translatesAutoresizingMaskIntoConstraints = true
        
        commentTextView.isUserInteractionEnabled = true
        commentTextView.frame = CGRect(x: 12, y: yAxisVal, width:Int(self.view.frame.size.width - 28), height: Int(heightVal))
        commentTextView.translatesAutoresizingMaskIntoConstraints = true
        commentTextView.scrollRangeToVisible(NSMakeRange(0, 0))
        
    }
    
    func heightOfLocNTranscCardCell(indexPath:IndexPath) -> CGFloat
    {
        let card = feedData[indexPath.row]
        var heightOfLocCard = CGFloat(185)
        
        let myString: NSString = card["update_comment"].stringValue as NSString
        let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
    
        if size.width > 325
        {
            heightOfLocCard = heightOfLocCard + CGFloat(size.height) * 2 + CGFloat(30)
        }
        else if size.width < 325 && size.width != 0
        {
            heightOfLocCard = heightOfLocCard + CGFloat(size.height) + CGFloat(30)
        }
        else
        {
            heightOfLocCard = heightOfLocCard + CGFloat(30)
        }
        if card["meta_data"]["account_name"].stringValue.characters.count == 0 && card["update_comment"].stringValue.characters.count == 0
        {
            heightOfLocCard = heightOfLocCard - CGFloat(30)
        }
        if (card["user_actions"].array?.count)! != 0
        {
            heightOfLocCard = heightOfLocCard + CGFloat(48)
        }
        
        return CGFloat(heightOfLocCard)
    }
    
    func dateLabelAlignment(datelabel:UILabel)
    {
        if datelabel.text?.characters.count == 1
        {
            datelabel.textAlignment = .right
        }
        else
        {
            datelabel.textAlignment = .center
        }
    }

    func handleRefresh(sender:AnyObject) {
        refreshControl.beginRefreshing()
        self.setUpFeedData()
    }
    
    func gotoEmptyFeedScreen() {
        let emptyFeedViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "EmptyFeedViewController") as? EmptyFeedViewController
        self.navigationController?.pushViewController(emptyFeedViewControllerObj!, animated: false)
    }
}
