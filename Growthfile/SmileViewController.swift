//
//  SmileViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 27/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SmileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,SelectedDataDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
    
    
    var nameLabel = UILabel()
    var dataArray = NSMutableArray()
    var delegate:SelectedDataDelegate?
    var backgroundUIView = UIView()
  
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var headerView: UIView!
    
    var selectedUserId = Int32()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       
        
        tableView.delegate = self
        tableView.dataSource = self
        
        submitView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        submitView.layer.borderWidth = 1
        submitView.layer.cornerRadius = 15
        
        //Set border color on cancelview
        cancelView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        cancelView.layer.borderWidth = 1
        cancelView.layer.cornerRadius = 15
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "smileCellIdentifier", for: indexPath as IndexPath)
        nameLabel = cell.viewWithTag(12) as! UILabel
        
        if  (cell.viewWithTag(1) != nil)
        {
            let deletebutton = cell.viewWithTag(1) as! UIButton
            deletebutton.tag = indexPath.row + 1
        }
        
        backgroundUIView = cell.viewWithTag(10)!
        backgroundUIView.layer.cornerRadius = 13
        backgroundUIView.clipsToBounds = true
        backgroundUIView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        backgroundUIView.layer.borderWidth = 1
        
        
        let receiverNameFirstCharacterLabel = cell.viewWithTag(11) as! UILabel
        receiverNameFirstCharacterLabel.layer.cornerRadius = receiverNameFirstCharacterLabel.frame.size.width/2
        receiverNameFirstCharacterLabel.clipsToBounds = true
        
        if dataArray.count == 0
        {
            backgroundUIView.isHidden = true
        }
        else
        {
            backgroundUIView.isHidden = false
            
            let fullName = nameLabel.text
            let fullNameArr = fullName?.characters.split{$0 == " "}.map(String.init)
            
            var str = String(describing: nameLabel.text!.characters.first!).capitalized
            
            let isIndexValid = fullNameArr?.indices.contains(1)
            
            if isIndexValid == true {
                let lastName:String = fullNameArr![1]
                str = String(describing: nameLabel.text!.characters.first!).capitalized +  String(describing: lastName.characters.first!).capitalized
            }
            
            receiverNameFirstCharacterLabel.text! =  str
            
        }
        
        return cell
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 52
    }
    
    
    func selectedDataArrayFromSearchViewController(selectedDataArray: NSMutableArray)
    {
        let selectedObject = selectedDataArray.object(at: 0) as! SearchDataModel
        nameLabel.text = selectedObject.name
        self.selectedUserId = selectedObject.id
        
        dataArray = selectedDataArray
        
        nameLabel.text = selectedObject.name
        let myString: NSString = nameLabel.text! as NSString
        let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        
        backgroundUIView.frame = CGRect(x: 48, y: 10, width: size.width + 32, height: 25)
        backgroundUIView.translatesAutoresizingMaskIntoConstraints = true
        self.tableView.reloadData()
    }

    
    
    @IBAction func moveToSearchViewcontrollerAction(_ sender: Any)
    {
        let searchViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
        searchViewControllerObj?.isSelectDataNNotMoveToProfileScreenBool = true
        searchViewControllerObj?.isStarOrSmileySearch = true
        searchViewControllerObj!.delegate = self
        self.navigationController?.pushViewController(searchViewControllerObj!, animated: false)
    }
    
    @IBAction func cancelAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func submitAction(_ sender: Any)
    {
        self.sendSmiley()
    }
    
    func sendSmiley()
    {
      loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        if self.selectedUserId > 0 {
            let params = [
                "receiver_id": self.selectedUserId
            ]
            
            Alamofire.request("\(Constants.domain)employees/sendsmiley", method: .post, parameters: params, headers: headers)
                .responseJSON { response in
                    print(response.request)  // original URL request
                    print(response.result)   // result of response serialization
                    switch response.result
                    {
                    case .success:
                        if let JSONdata = response.result.value {
                            print("JSON: \(JSONdata)")
                            let json = JSON(JSONdata)
                            if json["msg"] != "" {
                                let message = json["msg"].stringValue;
                                if message == "Token expired" || message == "Token invalid" {
                                    defaultVal.set(false, forKey: Constants.authUserKey)
                                }
                                if response.response?.statusCode == 200 {
                                    self.showSucessAlertMessage(msg: message)
                                }
                                else {
                                    self.showAlertMessage(msg: message)
                                }
                            }
                        }
                        self.loader.isHidden = true
                    case .failure(let error):
                        print(error)
                        self.loader.isHidden = true
                        self.showAlertMessage(msg: "Ops... Something went wrong")
                        return
                    }
            }
        } else {
            self.loader.isHidden = true
            self.showAlertMessage(msg: "Select a User")
        }
        
    }
    
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showSucessAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: cancelAction(_:)))
        self.present(alert, animated: true, completion: nil)
    }
}
