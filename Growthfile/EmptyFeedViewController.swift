//
//  HomeViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 22/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EmptyFeedViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var pressBackButton = Bool()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var otherView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        otherView.isHidden = true
        
        //Set shadow on headerview
        headerView.layer.shadowColor = UIColor.lightGray.cgColor
        headerView.layer.shadowOpacity = 0.2
        headerView.layer.shadowRadius = 2
        headerView.layer.shadowOffset = CGSize(width: -0.0, height: 2.0)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.setUpFeedData()
        
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        otherView.isHidden = true
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 6
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        
        if indexPath.row == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "growthfileIconIdentifier", for: indexPath as IndexPath)
        }
        else if indexPath.row == 1
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "instantReportIdentifier", for: indexPath as IndexPath)
        }
        else if indexPath.row == 2
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "meetingIdentifier", for: indexPath as IndexPath)
            
            let view = cell.viewWithTag(1)
            view?.layer.cornerRadius = (view?.frame.size.height)!/2
            view?.layer.masksToBounds = true
            view?.layer.borderColor = UIColor.darkGray.cgColor
            view?.layer.borderWidth = 1.0
            
        }
        else if indexPath.row == 3
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "invoiceIdentifier", for: indexPath as IndexPath)
            
            let view = cell.viewWithTag(1)
            view?.layer.cornerRadius = (view?.frame.size.height)!/2
            view?.layer.masksToBounds = true
            view?.layer.borderColor = UIColor.darkGray.cgColor
            view?.layer.borderWidth = 1.0
            
            let label = cell.viewWithTag(3) as! UILabel
            let myString: NSString = "amount just click on"
            let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)])
            label.frame = CGRect(x: 36,y:  27,width: size.width,height: size.height)
            
            label.translatesAutoresizingMaskIntoConstraints = true
            
            
        }
        else if indexPath.row == 4
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "thanksIdentifier", for: indexPath as IndexPath)
            
            let view = cell.viewWithTag(1)
            view?.layer.cornerRadius = (view?.frame.size.height)!/2
            view?.layer.masksToBounds = true
            view?.layer.borderColor = UIColor.darkGray.cgColor
            view?.layer.borderWidth = 1.0
            
            let label = cell.viewWithTag(2) as! UILabel
            let myString: NSString = "Say thank you or just a"
            let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)])
            label.frame = CGRect(x: 36,y:  10,width: size.width,height: size.height)
            
            label.translatesAutoresizingMaskIntoConstraints = true
            
        }
        else if indexPath.row == 5
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "congratsIdentifier", for: indexPath as IndexPath)
            
            let view = cell.viewWithTag(1)
            view?.layer.cornerRadius = (view?.frame.size.height)!/2
            view?.layer.masksToBounds = true
            view?.layer.borderColor = UIColor.darkGray.cgColor
            view?.layer.borderWidth = 1.0
            
            let label = cell.viewWithTag(2) as! UILabel
            let myString: NSString = "Congratulate a good job with a"
            let size: CGSize = myString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)])
            label.frame = CGRect(x: 36,y:  10,width: size.width,height: size.height)
            
            label.translatesAutoresizingMaskIntoConstraints = true
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 152
        }
        else if indexPath.row == 1
        {
            return 63
        }
        else if indexPath.row == 4 ||  indexPath.row == 5
        {
            return 47
        }
        else
        {
            return 60
        }
    }
    
    
    
    @IBAction func backbuttonAction(_ sender: Any)
    {
        if pressBackButton
        {
            pressBackButton = false
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func showLocationViewController(_ sender: Any)
    {
        let locationViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as? LocationViewController
        self.navigationController?.pushViewController(locationViewControllerObj!, animated: false)
    }
    
    @IBAction func showTransactionViewController(_ sender: Any)
    {
        let transactionViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as? TransactionViewController
        self.navigationController?.pushViewController(transactionViewControllerObj!, animated: false)
    }
    
    @IBAction func showStarViewControler(_ sender: Any)
    {
        let starViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "StarViewController") as? StarViewController
        self.navigationController?.pushViewController(starViewControllerObj!, animated: false)
    }
    
    @IBAction func showSmileViewController(_ sender: Any)
    {
        let smileViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SmileViewController") as? SmileViewController
        self.navigationController?.pushViewController(smileViewControllerObj!, animated: false)
    }
    
    @IBAction func moreViewAction(_ sender: Any)
    {
        otherView.isHidden = false
    }
    
    @IBAction func showPresentViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let applyNPresentViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ApplyNPresentViewController") as? ApplyNPresentViewController
        applyNPresentViewControllerObj?.headerTitle = "Present"
        self.navigationController?.pushViewController(applyNPresentViewControllerObj!, animated: false)
    }
    
    @IBAction func ShowLeaveViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let applyNPresentViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ApplyNPresentViewController") as? ApplyNPresentViewController
        applyNPresentViewControllerObj?.headerTitle = "Leave"
        self.navigationController?.pushViewController(applyNPresentViewControllerObj!, animated: false)
        
    }
    
    @IBAction func ShowResignViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let resignViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ResignViewController") as? ResignViewController
        self.navigationController?.pushViewController(resignViewControllerObj!, animated: false)
    }
    
    @IBAction func showSearchViewControllerAction(_ sender: Any) {
        otherView.isHidden = true
        let searchViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
        searchViewControllerObj?.isSelectDataNNotMoveToProfileScreenBool = false
//        searchViewControllerObj!.delegate = self
        self.navigationController?.pushViewController(searchViewControllerObj!, animated: false)
    }
    
    
    
    public func setUpFeedData()
    {
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        Alamofire.request("\(Constants.domain)get/employees/cards", method: .get, headers: headers).validate(statusCode: 200..<450)
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        let json = JSON(JSONdata)
                        if json["response"]["data"].count > 0 {
                            _ = self.navigationController?.popViewController(animated: false)
                        }
                    }
                    
                case .failure(let error):
                    print(error)
                }
        }
        
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
