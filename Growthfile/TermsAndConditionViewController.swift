//
//  TermsAndConditionViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 21/12/16.
//  Copyright © 2016 can. All rights reserved.


import UIKit

class TermsAndConditionViewController: UIViewController,UIWebViewDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
    var Url = String()
    var headerTitle = String()
    
    
    @IBOutlet weak var viewControllerTitle: UILabel!
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        webView.delegate = self
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        //Open webview
        webView.loadRequest(NSURLRequest(url: NSURL(string: Url)! as URL) as URLRequest)
        
        //Set shadow on headerview
        headerView.layer.shadowColor = UIColor.lightGray.cgColor
        headerView.layer.shadowOpacity = 0.2
        headerView.layer.shadowRadius = 2
        headerView.layer.shadowOffset = CGSize(width: -0.0, height: 2.0)
        
        viewControllerTitle.text = headerTitle
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView)
    {
        loader.isHidden = true
    }

    @IBAction func backAction(_ sender: Any)
    {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
