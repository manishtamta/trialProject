//
//  Constants.swift
//  Growthfile
//
//  Created by Chandresh SIngh on 28/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import Foundation

let defaultVal =  UserDefaults.standard
struct Constants
{
    static let domain = "https://backend.growthfile.com/api/v2/"
    static let tokenKey = "userToken"
    static let authUserKey = "authorizedUser"
    static let loggedInUserIdKey = "userId"
    static let userFCMTokenKey = "FCMToken"
    static let growthfileTermsAndConditionsSiteUrl = "https://growthfile.com/terms.php"
    static let growthfilePrivacyPolicySiteUrl = "https://growthfile.com/privacy.php"
}
