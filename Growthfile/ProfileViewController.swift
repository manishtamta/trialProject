//
//  ProfileViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 23/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Cloudinary

class ProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,TOCropViewControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,CLUploaderDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
 
    
    var selectedImage :UIImage? 
    var profileImageview = UIImageView()
    var collectionView :UICollectionView!
    var starNameArray = NSMutableArray()
    var collectionViewHeight = Int()
    var Cloudinary:CLCloudinary!
   
    @IBOutlet weak var otherView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var chooseNOptionLabel: UILabel!
    @IBOutlet weak var changeImageAlertView: UIView!
    @IBOutlet weak var changeImageBackgroundView: UIView!
   
    var userId = Int32()
    var isPreviousScreenHomeScreen = Bool()
    var profileData: JSON = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        self.setUpUserProfile()
        
        tableview.delegate = self
        tableview.dataSource = self
        otherView.isHidden = true
        
        changeImageAlertView.isHidden = true
        changeImageBackgroundView.isHidden = true
        
        chooseNOptionLabel.layer.cornerRadius = 13
        chooseNOptionLabel.clipsToBounds = true
        chooseNOptionLabel.layer.borderWidth = 1
        chooseNOptionLabel.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.38).cgColor
        
        changeImageAlertView.layer.cornerRadius = 13
        changeImageAlertView.clipsToBounds = true
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.delegate = self
        collectionView.dataSource = self
        
        let alertBackgroundViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.hideAlertOnClick(sender:)))
        changeImageBackgroundView.addGestureRecognizer(alertBackgroundViewTapGesture)
        
        //starNameArray = []
        collectionViewHeight = 0
        
        Cloudinary = CLCloudinary(url: "cloudinary://414334488441358:QMAHh5GTTLN1mA12jAw0VwcYz4o@growthfile")

    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        otherView.isHidden = true
    }
    

    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if profileData != [] {
            if (profileData["received_star_cards"].array?.count)! > 0 {
                return (profileData["received_star_cards"].array?.count)! + 4
            } else {
                return 3
            }
        } else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell()
        
        if indexPath.row == 0
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "profileCellIdentifier", for: indexPath as IndexPath)
            let profileBackgroungview = cell.viewWithTag(1)
            profileBackgroungview?.layer.cornerRadius = (profileBackgroungview?.frame.height)!/2
            
            profileImageview = cell.viewWithTag(2) as! UIImageView
            profileImageview.layer.cornerRadius = (profileImageview.frame.height)/2
            profileImageview.clipsToBounds = true
            
        
            if profileData != [] {
                
                let userId = profileData["id"].stringValue
                if userId != defaultVal.string(forKey: Constants.loggedInUserIdKey) {
                    let editImage = cell.viewWithTag(101)
                    editImage?.isHidden = true
                    let editImageButton = cell.viewWithTag(102) as! UIButton
                    editImageButton.isEnabled = false
                }
                
                let name = cell.viewWithTag(70) as! UILabel
                name.text = profileData["basic_profile"]["name"].stringValue
                
                let nameLabel = cell.viewWithTag(656) as! UILabel
                nameLabel.textColor = UIColor.white

                let profileImageString = profileData["basic_profile"]["pic"].stringValue
                commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: nameLabel, profileImageview: profileImageview, imageString: profileImageString, nameString: name.text!)
                
                let designation = cell.viewWithTag(4) as! UILabel
                designation.text = profileData["basic_profile"]["designation"].stringValue
                
                let doj = cell.viewWithTag(6) as! UILabel
                doj.text = profileData["basic_profile"]["date_of_joining"].stringValue
                
                let dept = cell.viewWithTag(7) as! UILabel
                dept.text = profileData["basic_profile"]["department"].stringValue
                
                if profileData["reports_to"]["name"] != JSON.null {
                    let manager = cell.viewWithTag(8) as! UILabel
                    manager.text = profileData["reports_to"]["name"].stringValue
                }
                
                let loc = cell.viewWithTag(9) as! UILabel
                loc.text = profileData["basic_profile"]["office_name"].stringValue
            }
        }
        else if indexPath.row == 1
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "locNsmileyIdentifier", for: indexPath as IndexPath)
            let firstView = cell.viewWithTag(1)
            firstView?.layer.shadowColor = UIColor.lightGray.cgColor
            firstView?.layer.shadowOpacity = 0.2
            firstView?.layer.shadowRadius = 2
            firstView?.layer.shadowOffset = CGSize(width: -0.0, height: 2.0)
            
            let secondView = cell.viewWithTag(2)
            secondView?.layer.shadowColor = UIColor.lightGray.cgColor
            secondView?.layer.shadowOpacity = 0.2
            secondView?.layer.shadowRadius = 2
            secondView?.layer.shadowOffset = CGSize(width: -0.0, height: 2.0)
            
            let onTimeLabel = cell.viewWithTag(101) as! UILabel
            onTimeLabel.text = profileData["attendance_summary"]["on_time"].stringValue
            let markedLabel = cell.viewWithTag(102) as! UILabel
            markedLabel.text = profileData["attendance_summary"]["attended"].stringValue
            let totalLabel = cell.viewWithTag(103) as! UILabel
            totalLabel.text = profileData["attendance_summary"]["total"].stringValue
            let receivedLabel = cell.viewWithTag(104) as! UILabel
            receivedLabel.text = profileData["smileys_ledger"]["recd"].stringValue
            let sentLabel = cell.viewWithTag(105) as! UILabel
            sentLabel.text = profileData["smileys_ledger"]["given"].stringValue
            
           
        }
        else if indexPath.row == 2
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "amountIdentifier", for: indexPath as IndexPath)
            
            let mainView = cell.viewWithTag(1)
            mainView?.layer.shadowColor = UIColor.lightGray.cgColor
            mainView?.layer.shadowOpacity = 0.2
            mainView?.layer.shadowRadius = 2
            mainView?.layer.shadowOffset = CGSize(width: -0.0, height: 2.0)
            
            let transactionMonth = cell.viewWithTag(121) as! UILabel
            transactionMonth.text = profileData["transaction_month"].stringValue
            
            var collectionAmount = 0
            var paymentAmount = 0
            
            var collectionCount = 0
            var paymentCount = 0
            
            let transactions = profileData["latest_transactions"]
            for (_,transaction): (String, JSON) in transactions {
                if transaction["status"].stringValue == "Expense" {
                    paymentAmount += Int(transaction["amount"].stringValue)!
                    paymentCount = paymentCount + 1
                }
                else if transaction["status"].stringValue == "Revenue" {
                    collectionAmount += Int(transaction["amount"].stringValue)!
                    collectionCount = collectionCount + 1
                }
            }
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            
            let collectionAmmountLabel = cell.viewWithTag(101) as! UILabel
            collectionAmmountLabel.text = "₹\(numberFormatter.string(from: NSNumber(value: collectionAmount))!)"
            
            let paymentAmountLabel = cell.viewWithTag(102) as! UILabel
            paymentAmountLabel.text = "₹\(numberFormatter.string(from: NSNumber(value: paymentAmount))!)"
            
            let collectionCountLabel = cell.viewWithTag(103) as! UILabel
            collectionCountLabel.text = "\(collectionCount)"
            let paymentCountLabel = cell.viewWithTag(104) as! UILabel
            paymentCountLabel.text = "\(paymentCount)"
            
        }
        else if indexPath.row == 3
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "dataIdentifier", for: indexPath as IndexPath)
        }
        else if indexPath.row >= 4
        {
            cell = tableView.dequeueReusableCell(withIdentifier: "starCardCellIdentifier", for: indexPath as IndexPath)
            
            let image = cell.viewWithTag(1) as! UIImageView
            image.layer.cornerRadius = (image.frame.size.height)/2
            image.layer.masksToBounds = true
            

            let profileImageString = ""
            let receivedStar = profileData["received_star_cards"][indexPath.row - 4]
            
            let nameLabel = cell.viewWithTag(2) as! UILabel
            nameLabel.text = receivedStar["updater"]["name"].stringValue

            let profileNameFirstCharacterLabel = cell.viewWithTag(98) as! UILabel
            let supporterLabel = cell.viewWithTag(788) as! UILabel
            
            if receivedStar["supporters"] != JSON.null && (receivedStar["supporters"].array?.count)! > 0 {
                supporterLabel.text = "+\((receivedStar["supporters"].array?.count)!)"
            }
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.gotoSelectedUserProfileScreen(sender:)))
            let starCardheaderView = cell.viewWithTag(122)! as UIView
            tap.accessibilityHint = receivedStar["updater"]["id"].stringValue
            starCardheaderView.addGestureRecognizer(tap)
            starCardheaderView.isUserInteractionEnabled = true
            
            let actionLabel = cell.viewWithTag(3) as! UILabel
            actionLabel.text = receivedStar["top_right_text"].stringValue
            
            let designationLabel = cell.viewWithTag(4) as! UILabel
            designationLabel.text = receivedStar["updater"]["designation"].stringValue
            
            let timeLabel = cell.viewWithTag(5) as! UILabel
            let timestamp = receivedStar["updated_at"].stringValue

            
            if timestamp != ""
            {
                timeLabel.text = commonFunctionFileObj.timestampToCardTimeString(timestamp: timestamp)
            }
            
            let fromLabel = cell.viewWithTag(7) as! UILabel
            fromLabel.text = receivedStar["updater"]["name"].stringValue
            
            let toLabel = cell.viewWithTag(8) as! UILabel
            toLabel.isHidden = true
           
            collectionView = cell.viewWithTag(11) as! UICollectionView
            
            let commentView = cell.viewWithTag(76)
            
            let commentTextView = cell.viewWithTag(9) as! UITextView
            commentTextView.text = receivedStar["update_comment"].stringValue
            starNameArray = []
            let receivers = receivedStar["receivers"]
            for (_,receiver) in receivers {
                starNameArray.add(receiver["name"].stringValue)
            }

            print("vvvvvvvvv \(starNameArray)")
            
            for _ in starNameArray
            {
                collectionViewHeight = collectionViewHeight + 14
                collectionView.reloadData()
            }
            
            
            let toTextLabel = cell.viewWithTag(10) as! UILabel
            let toLabelViewContainer = cell.viewWithTag(12)
            if starNameArray.count > 3
            {
                toTextLabel.frame = CGRect(x:CGFloat((toLabelViewContainer!.bounds.size.width - toTextLabel.frame.size.width)/2)  , y: CGFloat(10) , width: toTextLabel.frame.size.width, height: toTextLabel.frame.size.height)
                toTextLabel.translatesAutoresizingMaskIntoConstraints = true
                self.collectionView.frame = CGRect(x: Int((toLabelViewContainer!.bounds.size.width - collectionView.frame.size.width)/2),y: 30,width: Int(collectionView.frame.size.width),height: collectionViewHeight)
                collectionView.translatesAutoresizingMaskIntoConstraints = true
            }
            else
            {
                print("collectionViewHeightcollectionViewHeightcollectionViewHeight \(collectionViewHeight)")
                toTextLabel.frame = CGRect(x:CGFloat((toLabelViewContainer!.bounds.size.width - toTextLabel.frame.size.width)/2)  , y: CGFloat(37) , width: toTextLabel.frame.size.width, height: toTextLabel.frame.size.height)
                toTextLabel.textAlignment = NSTextAlignment.center
                toTextLabel.translatesAutoresizingMaskIntoConstraints = true
                self.collectionView.frame = CGRect(x: Int((toLabelViewContainer!.bounds.size.width - collectionView.frame.size.width)/2),y: 57,width: Int(collectionView.frame.size.width),height: collectionViewHeight)
                collectionView.translatesAutoresizingMaskIntoConstraints = true
            }
            
            
            commonFunctionFileObj.addImageOrNameLabelValueToCard(profileLabel: profileNameFirstCharacterLabel, profileImageview: image, imageString: profileImageString, nameString: nameLabel.text!)
            
            adjustCommentviewAccordingToText(commentView: commentView!, commentTextView: commentTextView, indexPath: indexPath)
            
            collectionViewHeight = 0
        }
//        else if indexPath.row == 4
//        {
//            cell = tableView.dequeueReusableCell(withIdentifier: "dataIdentifier", for: indexPath as IndexPath)
//            let noDataLabel = cell.viewWithTag(1)
//            noDataLabel?.layer.borderWidth = 1
//            noDataLabel?.layer.borderColor = UIColor(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5).cgColor
//            noDataLabel?.layer.cornerRadius = 10
//        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 137
        }
        else if indexPath.row == 1
        {
            return 123
        }
        else if indexPath.row == 2
        {
            return 138
        }
        else if indexPath.row >= 4
        {
            //return 262
            return heightOfCardCell(indexPath: indexPath)
        }
        else if indexPath.row == 3
        {
            return 65
        }
        else
        {
            return 90
        }
    }
    
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        print("numberOfItemsInSectionnumberOfItemsInSection \(starNameArray.count)")
        return starNameArray.count
    }
    

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nameCellIdentifier", for: indexPath as IndexPath)
        
        let nameLabel = cell.viewWithTag(1) as! UILabel
        nameLabel.text = String(describing: self.starNameArray[indexPath.item])
        return cell
    }
    
    
    func imageWithImage (image:UIImage, scaledToWidth: CGFloat) -> UIImage
    {
        let oldWidth = image.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = image.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        let rect = CGRect(x: 0, y: 0, width: CGFloat(newWidth), height: CGFloat(newHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let compressedImageData = UIImageJPEGRepresentation(resizedImage!, 0.8)
        return UIImage(data: compressedImageData!)!
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let capturedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let compressedImage = imageWithImage(image: capturedImage, scaledToWidth: 700.0)
        selectedImage = compressedImage
        
        defaultVal.set(UIImagePNGRepresentation(selectedImage!), forKey: "profileImage")
        self.dismiss(animated: true, completion: nil)
        
        changeImageAlertView.isHidden = true
        changeImageBackgroundView.isHidden = true
        
        let cropController:TOCropViewController = TOCropViewController(image: selectedImage!)
        cropController.delegate = self
        self.present(cropController, animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: { () -> Void in })
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int)
    {
        cropViewController.dismiss(animated: true) { () -> Void in
            let imageData = UIImagePNGRepresentation(image)! as Data
            self.uploadToCloudinary(imageData: imageData as NSData)
            self.profileImageview.image = image
        }
    }
    
    func uploadToCloudinary(imageData:NSData){
        let uploader = CLUploader(Cloudinary, delegate: self)
        
        uploader?.upload(imageData, options: nil, withCompletion: { (successResult, errorResult, code, context) -> Void in
            if let url = successResult?["url"] as? String {
                self.updateUserProfileUrl(url: url)
            }

        }, andProgress: { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite, context) -> Void in
            //
        })
    }
    

    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool)
    {
        cropViewController.dismiss(animated: true) { () -> Void in  }
    }
    
    func hideAlertOnClick(sender:UITapGestureRecognizer)
    {
        changeImageAlertView.isHidden = true
        changeImageBackgroundView.isHidden = true
    }
    
    func checkisSourceTypeAvailable(errorMessage : String,sourceType : UIImagePickerControllerSourceType)
    {
        if UIImagePickerController.isSourceTypeAvailable(sourceType)
        {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = sourceType
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            self.showAlertMessage(msg: errorMessage)
        }
    }
    
    
    
    @IBAction func showGalleryButtonAction(sender: AnyObject)
    {
        checkisSourceTypeAvailable(errorMessage: "Something went wrong", sourceType: UIImagePickerControllerSourceType.photoLibrary)
    }
    
    @IBAction func showCameraButtonPostionAction(sender: AnyObject)
    {
        checkisSourceTypeAvailable(errorMessage: "Camera not available", sourceType: UIImagePickerControllerSourceType.camera)
    }
    @IBAction func editProfileImage(_ sender: Any)
    {
        changeImageAlertView.isHidden = false
        changeImageBackgroundView.isHidden = false
        self.changeImageBackgroundView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.9)
    }
    
    @IBAction func backButtonAction(_ sender: Any)
    {
        if isPreviousScreenHomeScreen == true {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
        }
        
    }
    
    @IBAction func showHomeViewController(_ sender: Any)
    {
        let homeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        homeViewControllerObj?.pressBackButton = true
        
        self.navigationController?.pushViewController(homeViewControllerObj!, animated: false)
    }

    @IBAction func showLocationViewController(_ sender: Any)
    {
        let locationViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as? LocationViewController
        self.navigationController?.pushViewController(locationViewControllerObj!, animated: false)
    }
   
    @IBAction func showTransactionViewController(_ sender: Any)
    {
        let transactionViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as? TransactionViewController
        self.navigationController?.pushViewController(transactionViewControllerObj!, animated: false)
    }
    
    @IBAction func showStarViewControler(_ sender: Any)
    {
        let starViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "StarViewController") as? StarViewController
        self.navigationController?.pushViewController(starViewControllerObj!, animated: false)
    }
    
    @IBAction func showSmileViewController(_ sender: Any)
    {
        let smileViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SmileViewController") as? SmileViewController
        self.navigationController?.pushViewController(smileViewControllerObj!, animated: false)
    }
   
    @IBAction func moreViewAction(_ sender: Any)
    {
      otherView.isHidden = false
    }

    @IBAction func showPresentViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let applyNPresentViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ApplyNPresentViewController") as? ApplyNPresentViewController
        applyNPresentViewControllerObj?.headerTitle = "Present"
        self.navigationController?.pushViewController(applyNPresentViewControllerObj!, animated: false)
    }
    
    @IBAction func ShowLeaveViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let applyNPresentViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ApplyNPresentViewController") as? ApplyNPresentViewController
        applyNPresentViewControllerObj?.headerTitle = "Leave"
        self.navigationController?.pushViewController(applyNPresentViewControllerObj!, animated: false)
    }
    
    @IBAction func ShowResignViewControllerAction(_ sender: Any)
    {
        otherView.isHidden = true
        let resignViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ResignViewController") as? ResignViewController
        self.navigationController?.pushViewController(resignViewControllerObj!, animated: false)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    public func setUpUserProfile()
    {
        loader.isHidden = false
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!

        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        var params = [String: Int32]()
        if userId > 0 {
            params["user_id"] = userId
        }
        
        Alamofire.request("\(Constants.domain)employees/growthfile", method: .get, parameters: params, headers: headers)
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["response"][0] != JSON.null
                        {
                            self.profileData = json["response"][0]
                            self.tableview.reloadData()
                            self.collectionView.reloadData()
                        } else if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            self.showAlertMessage(msg: message)
                        }
                    }
                 self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Invalid credentials")
                    return
                }
        }
        
    }
    
    func updateUserProfileUrl(url: String)
    {
        loader.isHidden = false
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        let params: Parameters = [
            "profile_image_url": url
        ]
        
        Alamofire.request("\(Constants.domain)employees/profileimage", method: .patch, parameters: params, headers: headers)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        let json = JSON(JSONdata)
                        if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            
                            self.showAlertMessage(msg: message)
                            if response.response?.statusCode == 200 {
                                self.setUpUserProfile()
                            }
                        }
                    }
                    self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Ops.. Something went wrong")
                    return
                }
        }
    }
    
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // Go to selected card user profile screen
    func gotoSelectedUserProfileScreen(sender: UITapGestureRecognizer) {
        let userId = sender.accessibilityHint
        if userId != nil && userId != "" {
            let profileViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            
            profileViewControllerObj?.userId = Int32(userId!)!
            profileViewControllerObj?.isPreviousScreenHomeScreen = true
            
            self.navigationController?.pushViewController(profileViewControllerObj!, animated: true)
        }
    }
    
    
    func getFirstCharacterFromProfileName(name:String) -> String
    {
        let fullNameArr = name.characters.split{$0 == " "}.map(String.init)
        
        var firstCharacterstring = String(describing: name.characters.first!).capitalized
        
        let isIndexValid = fullNameArr.indices.contains(1)
        
        if isIndexValid == true
        {
            let lastName:String = fullNameArr[1]
            firstCharacterstring = String(describing: name.characters.first!).capitalized +  String(describing: lastName.characters.first!).capitalized
        }
        
        return firstCharacterstring
    }
    
    func adjustCommentviewAccordingToText(commentView:UIView,commentTextView:UITextView,indexPath:IndexPath)
    {
        let card = profileData["received_star_cards"][indexPath.row - 4]
        
        if card["update_comment"].stringValue == ""
        {
            commentView.isHidden = true
        }
        else
        {
            commentView.isHidden = false
            commentTextView.text = card["update_comment"].stringValue
        }
        
        let commentString: NSString = card["update_comment"].stringValue as NSString
        let size: CGSize = commentString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        var height = CGFloat(0)
        
        if size.width > 325
        {
            height = CGFloat(size.height) + CGFloat(18)
        }
        else
        {
            height = (CGFloat(2) * CGFloat(size.height)) + CGFloat(18)
        }
        
        commentTextView.frame = CGRect(x: 14, y: 4, width: 347, height: height + CGFloat(7))
        commentTextView.translatesAutoresizingMaskIntoConstraints = true
        
        commentView.frame = CGRect(x: 0, y: 184, width: self.view.frame.size.width, height: height + CGFloat(21))
        commentView.translatesAutoresizingMaskIntoConstraints = true
        
    }
    
    func heightOfCardCell(indexPath:IndexPath) -> CGFloat
    {
        let card = profileData["received_star_cards"][indexPath.row - 4]
        var heightOfCard = CGFloat(185)
        
        let commentString: NSString = card["update_comment"].stringValue as NSString
        let size: CGSize = commentString.size(attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)])
        
        if size.width > 325
        {
            heightOfCard = heightOfCard + CGFloat(size.height) * 2 + CGFloat(18)
        }
        else if size.width >= 1
        {
            heightOfCard = heightOfCard + CGFloat(size.height) + CGFloat(18)
        }
        
        if (card["user_actions"].array?.count)! != 0
        {
            heightOfCard = heightOfCard + CGFloat(48)
        }
        
        return CGFloat(heightOfCard)
    }

}
