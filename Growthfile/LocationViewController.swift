//
//  LocationViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 23/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import SwiftyJSON

class LocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,UITextViewDelegate
{
    //Variable and constant for activityIndicator
    var url = NSURL()
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
    let searchField = UITextField()
    var mapView = GMSMapView()
    var crossImage = UIImageView()
    var dropDownImage = UIImageView()
    var commentTextview = UITextView()
    var selectAmountLabel = UILabel()
    var amountNameArray = NSMutableArray()
    var filterdItemsArray = NSMutableArray()
    var locationManager = CLLocationManager()
    var selectAmountTableViewPopUp = UITableView()
    
    var searchCharecter = Bool()
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var submitView: UIView!
    
    var accountsList : JSON = []
    var currentLocation = CLLocation()
    var selectedAccountId = Int16();
    
    var isdropDownButtonClicked = false
    
    override func viewDidLoad()
    {
        
        searchCharecter = false
        if (CLLocationManager.locationServicesEnabled())
        {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                self.showSucessAlertMessage(msg: "Please enable location service through Settings");
            default:
                break
            }
        }
        selectAmountTableViewPopUp.delegate = self
        selectAmountTableViewPopUp.dataSource = self
        
        searchField.addTarget(self, action: #selector(LocationViewController.edit), for: .editingChanged)
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        if locationManager.location != nil {
            self.currentLocation = locationManager.location!
        } else {
            self.showSucessAlertMessage(msg: "Unable to determine your location");
        }
        
        super.viewDidLoad()
        
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        self.setAccountsList()
        
        tableView.delegate = self
        tableView.dataSource = self
        commentTextview.delegate = self
        crossImage.isHidden = true
        
        //Set border color on submitView
        submitView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        submitView.layer.borderWidth = 1
        submitView.layer.cornerRadius = 15
        
        //Set border color on cancelview
        cancelView.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        cancelView.layer.borderWidth = 1
        cancelView.layer.cornerRadius = 15
        
        selectAmountTableViewPopUp.tag = 100
        selectAmountTableViewPopUp.rowHeight = 30
        selectAmountTableViewPopUp.delegate = self
        selectAmountTableViewPopUp.dataSource = self
        selectAmountTableViewPopUp.layer.borderWidth = 1
        selectAmountTableViewPopUp.isScrollEnabled = true
        selectAmountTableViewPopUp.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
        self.tableView.addSubview(selectAmountTableViewPopUp)
        
        selectAmountTableViewPopUp.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        searchField.delegate = self as? UITextFieldDelegate
        
    }
    
    
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (tableView.tag == 100)
        {
            
            if searchCharecter == true{
                return filterdItemsArray.count
            }
            return amountNameArray.count
        }
        
        return 3
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell: UITableViewCell?
        if (tableView.tag == 100)
        {
            if (cell == nil)
            {
                cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "popUpIdentifier")
            }
            
            if searchCharecter == true {
                let model = filterdItemsArray.object(at: indexPath.row) as! SelectAmountDataModel
                cell?.textLabel?.text = String(model.accountName)!
                cell?.textLabel?.textColor = UIColor.darkGray
                cell?.textLabel?.font = UIFont(name: "System", size: 12)
            }else{
                let model = amountNameArray.object(at: indexPath.row) as! SelectAmountDataModel
                cell?.textLabel?.text = String(model.accountName)!
                cell?.textLabel?.textColor = UIColor.darkGray
                cell?.textLabel?.font = UIFont(name: "System", size: 12)
            }
        }
        else
        {
            if indexPath.row == 0
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "mapIdentifier", for: indexPath as IndexPath)
                let locationImage = cell?.viewWithTag(1) as! UIImageView
                if url.absoluteString != nil{
                    locationImage.sd_setImage(with: (url as NSURL) as URL!)
                }
            }
            else if indexPath.row == 1
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "accountIdentifier", for: indexPath as IndexPath)
                selectAmountLabel = cell?.viewWithTag(1) as! UILabel
                let showDropDownButton = cell?.viewWithTag(2) as! UIButton
                showDropDownButton.addTarget(self, action: #selector(LocationViewController.showDropDownButtonAction), for: UIControlEvents.touchUpInside)
                
                dropDownImage = cell?.viewWithTag(3) as! UIImageView
                crossImage = cell?.viewWithTag(4) as! UIImageView
                
                if selectAmountTableViewPopUp.isHidden == false{
                    searchField.frame = CGRect(x: 0, y: 0, width: showDropDownButton.frame.size.width-35, height: showDropDownButton.frame.size.height)
                    searchField.isHidden = true
                    //                    searchField.backgroundColor = UIColor.clear
                    //                    searchField.borderStyle = .none
                    //  searchField.becomeFirstResponder()
                    showDropDownButton.addSubview(searchField)
                }else{
                    searchField.removeFromSuperview()
                    searchCharecter = false
                }
            }
            else if indexPath.row == 2
            {
                cell = tableView.dequeueReusableCell(withIdentifier: "commentIdentifier", for: indexPath as IndexPath)
                
                commentTextview = cell?.viewWithTag(1) as! UITextView
                commentTextview.layer.cornerRadius = 5;
                commentTextview.layer.borderWidth = 1
                commentTextview.layer.borderColor = UIColor(red: 184/255.0, green: 184/255.0, blue: 184/255.0, alpha: 0.5).cgColor
            }
        }
        return cell!
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (tableView.tag == 100)
        {
            return 30
        }
        else
        {
            if indexPath.row == 0
            {
                return 170
            }
            else if indexPath.row == 1
            {
                return 44
            }
            else
            {
                return 149
            }
        }
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (tableView.tag == 100)
        {
            let model = amountNameArray.object(at: indexPath.row) as! SelectAmountDataModel
            selectAmountLabel.text = String(model.accountName)!
            searchField.isHidden = true
            self.selectedAccountId = model.accountId
            selectAmountTableViewPopUp.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    
    public func textViewDidBeginEditing(_ textView: UITextView)
    {
        selectAmountTableViewPopUp.isHidden = true
        if textView.text == "Write a comment..."
        {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty
        {
            textView.text = "Write a comment..."
            textView.textColor = UIColor.darkGray
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"
        {
            textView.endEditing(true)
            return false
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count
        return numberOfChars <= 200
    }
    
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(UIScreen.main.bounds.size.height <= 568.0)
        {
            self.tableView.contentOffset = CGPoint(x: 0, y: 40)
            self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height+40)
        }
    }
    
    
    func keyboardWillHide(notification: NSNotification)
    {
        if(UIScreen.main.bounds.size.height <= 568.0)
        {
            self.tableView.contentOffset = CGPoint(x: 0, y: 0)
            self.tableView.contentSize = CGSize(width: self.tableView.frame.size.width, height: self.tableView.frame.size.height)
        }
    }
    
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        locationManager.stopUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        
        DispatchQueue.main.async {
            self.url = NSURL(string: "https://maps.googleapis.com/maps/api/staticmap?center=\(coord.latitude),\(coord.longitude)&markers=\(coord.latitude),\(coord.longitude)&zoom=17&size=\(Int(UIScreen.main.bounds.size.width))x164&maptype=roadmap&format=png")!

            self.tableView.reloadData()
            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.loader.isHidden = true
            }
            
        }
        
        if locationManager.location != nil{
            self.currentLocation = locationManager.location!
        }
        locationManager.stopUpdatingLocation()
    }
    
    func showDropDownButtonAction()
    {
        if amountNameArray.count == 0 {
            selectAmountLabel.text = "No Account Exists"
        }
        else {
            if isdropDownButtonClicked == true {
                self.isdropDownButtonClicked = false
                dropDownImage.isHidden = false
                crossImage.isHidden = true
                
                //            selectAmountLabel.text = nil
                selectAmountLabel.text = "Select Account"
                
                selectAmountTableViewPopUp.isHidden = true
                tableView.reloadData()
            }
            else {
                self.isdropDownButtonClicked = true
                dropDownImage.isHidden = true
                crossImage.isHidden = false
                
                var tableHeight = 0
                if amountNameArray.count < 5
                {
                    tableHeight = amountNameArray.count * 30
                }
                else
                {
                    tableHeight = 150
                }
                
                let selectAmountLabelOriginInTableView =  selectAmountLabel.convert(selectAmountLabel.frame.origin, to: tableView)
                selectAmountTableViewPopUp.frame =  CGRect(x: 93, y: selectAmountLabelOriginInTableView.y + selectAmountLabel.frame.size.height + 12, width: selectAmountLabel.frame.size.width + 45, height: CGFloat(tableHeight))
                selectAmountTableViewPopUp.isHidden = false
                tableView.reloadData()
                
            }
        }
    }
    
    @IBAction func cancelAction(_ sender: Any)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: Any)
    {
        self.markLocation()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func edit() -> Void {
        print("edited ",searchField.text!.characters.count)
        //  selectAmountLabel.text = searchField.text!
        
        if searchField.text!.characters.count > 0{
            selectAmountLabel.isHidden = true
            for i in 0...amountNameArray.count-1{
                
                var model = SelectAmountDataModel()
                model = amountNameArray.object(at: i) as! SelectAmountDataModel
                
                if ((model.accountName).lowercased().range(of: searchField.text!.lowercased()) != nil) {
                    self.filterdItemsArray.add(amountNameArray[i])
                }else{
                    self.filterdItemsArray.removeAllObjects()
                }
                searchCharecter = true
                selectAmountTableViewPopUp.reloadData()
                
            }
        }else{
            self.filterdItemsArray.removeAllObjects()
            searchCharecter = false
            selectAmountLabel.isHidden = false
            selectAmountTableViewPopUp.reloadData()
        }
        
    }
    
    
    
    public func setAccountsList()
    {
        self.loader.isHidden = false
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        print(token)
        if token == "" {
            // show error message
        }
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        
        Alamofire.request("\(Constants.domain)get/list/accounts", method: .get, headers: headers)
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    
                    if let JSONdata = response.result.value
                    {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["response"]["data"] != JSON.null
                        {
                            self.accountsList = json["response"]["data"]
                            
                            for (_,account):(String, JSON) in self.accountsList {
                                let model = SelectAmountDataModel()
                                model.accountName = account["account_name"].stringValue
                                model.accountId = Int16(account["id"].stringValue)!
                                self.amountNameArray.add(model)
                            }
                        } else if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            if message != "Object not found." {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                    
                //  self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    //    self.loader.isHidden = true
                    self.showAlertMessage(msg: "Invalid credentials")
                    return
                }
        }
        
    }
    
    
    func markLocation()
    {
        loader.isHidden = false
        
        let token = defaultVal.string(forKey: Constants.tokenKey)!
        
        let headers: HTTPHeaders = [
            "Authorization": "bearer \(token)"
        ]
        
        var params = [
            "lat": String(self.currentLocation.coordinate.latitude),
            "lng": String(self.currentLocation.coordinate.longitude),
            "device_id": "ios device id"
        ]
        
        print(params)
        
        let comment = self.commentTextview.text
        if comment != "Write a comment..."
        {
            params["comment"] = comment
        }
        
        if selectAmountLabel.text!.characters.count > 0 && selectAmountLabel.text! != "Select Account"{
            if self.selectedAccountId > 0
            {
                params["account_id"] = String(self.selectedAccountId)
            }
        }
        
        Alamofire.request("\(Constants.domain)employees/marklocation", method: .post, parameters: params, headers: headers)
            .responseJSON
            { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value
                    {
                        let json = JSON(JSONdata)
                        if json["msg"] != "" {
                            let message = json["msg"].stringValue;
                            if message == "Token expired" || message == "Token invalid" {
                                defaultVal.set(false, forKey: Constants.authUserKey)
                            }
                            if response.response?.statusCode == 200 {
                                self.showSucessAlertMessage(msg: message)
                            }
                            else {
                                self.showAlertMessage(msg: message)
                            }
                        }
                    }
                    self.loader.isHidden = true
                    
                case .failure(let error):
                    print(error)
                    self.showAlertMessage(msg: "Ops... Something went wrong")
                    self.loader.isHidden = true
                    return
                }
        }
        
    }
    
    
    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func goBackToPreviousScreen(action: UIAlertAction)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showSucessAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: goBackToPreviousScreen(action: )))
        self.present(alert, animated: true, completion: nil)
    }
    
}
