//
//  OtpViewController.swift
//  Growthfile
//
//  Created by Preeti Sharma on 21/12/16.
//  Copyright © 2016 can. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class OtpViewController: UIViewController,UITextFieldDelegate
{
    //Variable and constant for activityIndicator
    let commonFunctionFileObj = CommonFunctionFile()
    var loader = UIView()
   
    
    let window = UIWindow()
    var mobileNumber = String()
    var currentTextField = UITextField()
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var otpFirstTextfield: UITextField!
    @IBOutlet weak var otpSecTextfield: UITextField!
    @IBOutlet weak var otpThirdTextfield: UITextField!
    @IBOutlet weak var otpFourthTextfield: UITextField!
    @IBOutlet weak var otpFifthTextfield: UITextField!
    @IBOutlet weak var otpSixTextfield: UITextField!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       
      
        otpFirstTextfield.addTarget(self, action: #selector(OtpViewController.textFieldCursorMoveToNext(textField:)), for: UIControlEvents.editingChanged)
        otpSecTextfield.addTarget(self, action: #selector(OtpViewController.textFieldCursorMoveToNext(textField:)), for: UIControlEvents.editingChanged)
        otpThirdTextfield.addTarget(self, action: #selector(OtpViewController.textFieldCursorMoveToNext(textField:)), for: UIControlEvents.editingChanged)
        otpFourthTextfield.addTarget(self, action: #selector(OtpViewController.textFieldCursorMoveToNext(textField:)), for: UIControlEvents.editingChanged)
        otpFifthTextfield.addTarget(self, action: #selector(OtpViewController.textFieldCursorMoveToNext(textField:)), for: UIControlEvents.editingChanged)
        otpSixTextfield.addTarget(self, action: #selector(OtpViewController.textFieldCursorMoveToNext(textField:)), for: UIControlEvents.editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        //Show keyboard when view did appear
        otpFirstTextfield.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        //Hide keyboard when view did disappear
        currentTextField.resignFirstResponder()
    }
    
    
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        
        let newLength = text.utf16.count + string.utf16.count - range.length
        return newLength <= 1
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        currentTextField = textField
        return true
    }
    
    
    
    func textFieldCursorMoveToNext(textField: UITextField)
    {
        //Handle cursor movement on textfield
        let text = textField.text
        
        if text?.utf16.count==1
        {
            switch textField
            {
            case otpFirstTextfield:
                otpSecTextfield.becomeFirstResponder()
            case otpSecTextfield:
                otpThirdTextfield.becomeFirstResponder()
            case otpThirdTextfield:
                otpFourthTextfield.becomeFirstResponder()
            case otpFourthTextfield:
                otpFifthTextfield.becomeFirstResponder()
            case otpFifthTextfield:
                otpSixTextfield.becomeFirstResponder()
            case otpSixTextfield:
                otpSixTextfield.becomeFirstResponder()
            default:
                break
            }
        }
        else
        {
            switch textField
            {
            case otpFirstTextfield:
                otpFirstTextfield.becomeFirstResponder()
            case otpSecTextfield:
                otpFirstTextfield.becomeFirstResponder()
            case otpThirdTextfield:
                otpSecTextfield.becomeFirstResponder()
            case otpFourthTextfield:
                otpThirdTextfield.becomeFirstResponder()
            case otpFifthTextfield:
                otpFourthTextfield.becomeFirstResponder()
            case otpSixTextfield:
                otpFifthTextfield.becomeFirstResponder()
            default:
                break
            }
            
        }
    }

    func showAlertMessage(msg:String)
    {
        let alert = UIAlertController(title:nil , message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func pushToTabBarViewcontroller(_ sender: Any)
    {
        let otpString = (((((otpFirstTextfield.text! + otpSecTextfield.text!) + otpThirdTextfield.text! ) + otpFourthTextfield.text!) + otpFifthTextfield.text!) + otpSixTextfield.text!)
        
        if otpString.characters.count == 6
        {
            currentTextField.resignFirstResponder()
            login(mobile: self.mobileNumber, otp: otpString)
        }
        else
        {
           self.showAlertMessage(msg: "Invalid otp")
        }
    }
    
    
    @IBAction func resendOTPButtonAction(_ sender: UIButton) {
        self.resendOTP()
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
 
    public func login(mobile: String, otp: String)
    {
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        let fcmToken = defaultVal.string(forKey: Constants.userFCMTokenKey)
        
        var params: Parameters = [
            "mobile": mobile,
            "otp": otp,
            "app_id": "gfv2",
            "device_id": "device id",
            "device_model": "iphone 6s",
            "os_type": "IOS",
            "os_version": "10.1",
            "device_type": "IOS"
        ]
        
        if fcmToken != nil {
            params["device_token"] = fcmToken!
        }
        
        Alamofire.request("\(Constants.domain)employees/validateotp", method: .post, parameters: params)
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    self.loader.isHidden = true
                    
                    if let JSONdata = response.result.value {
                        print("JSON:3434443 \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["token"] != JSON.null
                        {
                            let token = json["token"].stringValue
                            print(token)
                            
                            // Store user token
                            defaultVal.set(true, forKey: Constants.authUserKey)
                            defaultVal.set(token, forKey: Constants.tokenKey)
                            defaultVal.set(json["response"]["id"].stringValue, forKey: Constants.loggedInUserIdKey)
                            
                            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                            do {
                                let data = try self.context.fetch(request) as! [User]
                                for result in data {
                                    self.context.delete(result)
                                }
                                
                                let user = NSEntityDescription.insertNewObject(forEntityName: "User", into: self.context) as! User
                                if json["response"]["name"] != JSON.null {
                                    user.name = json["response"]["name"].stringValue
                                }
                                if json["response"]["profile_image"] != JSON.null {
                                    user.profile_image = json["response"]["profile_image"].stringValue
                                }
                                user.mobile = self.mobileNumber
                                user.token = token
                                
                                let data2 = try self.context.fetch(request) as! [User]
                                print(data2[0])
                            
                                self.continueToTabBarView()
                                
                            }catch let error as NSError {
                                print("Unable to fetch \(error)")
                            }
                        }
                        else if json["msg"] != JSON.null {
                            self.showAlertMessage(msg: json["msg"].stringValue)
                        }
                    }
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    if error._code == -1009 {
                        self.showAlertMessage(msg: error.localizedDescription)
                    } else {
                        self.showAlertMessage(msg: "Ops... Something went wrong")
                    }
                    return
                }
        }
    }
    
    
    
    public func resendOTP()
    {
        loader =  commonFunctionFileObj.createLoader(view: self.view)
        
        let params = [
            "mobile": self.mobileNumber
        ]
        
        Alamofire.request("\(Constants.domain)employees/generateotp", method: .patch, parameters: params)
            .responseJSON { response in
                print(response.request)  // original URL request
                print(response.result)   // result of response serialization
                switch response.result
                {
                case .success:
                    if let JSONdata = response.result.value {
                        print("JSON: \(JSONdata)")
                        let json = JSON(JSONdata)
                        if json["msg"] != JSON.null {
                            if json["msg"].stringValue == "" {
                                self.showAlertMessage(msg: "OTP Sent")
                            }
                        }
                    }
                self.loader.isHidden = true
                case .failure(let error):
                    print(error)
                    self.loader.isHidden = true
                    
                    if error._code == -1009 || error._code == -4 {
                        self.showAlertMessage(msg: error.localizedDescription)
                    } else {
                        self.showAlertMessage(msg: "Ops... Something went wrong")
                    }
                    
                    return
                }
        }
        
    }
    
    
    public func continueToTabBarView() {
        let homeViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        self.navigationController?.pushViewController(homeViewControllerObj!, animated: false)
    }
}
